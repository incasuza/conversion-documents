//#region Imports
const Wizard = require('./web-wizard/classes/wizard.js');
//#endregion
//#region Wizard Object
const app = new Wizard.App({ wwWizardData: wwWizardData, webServiceUrl: `${environmentContext.Url.BusinessLayer}`, primaryColor: 'rgb(223, 39, 39)' });
//#endregion
//#region Web & Data Calls
function DataCall(id, query) {
    app.addDataCall({
            type: environmentContext.Database.Mentis.type, data: {
            id: id,
            user: environmentContext.Database.Mentis.user,
            password: environmentContext.Database.Mentis.password,
            server: environmentContext.Database.Mentis.server,
            port: environmentContext.Database.Mentis.port,
            database: environmentContext.Database.Mentis.dbname,
            query: query
        }
    });
}
function WebDataCall(id, url){
    app.addDataCall({
        type: 'web', data: {
            id: id,
            method: 'post',
            url: url,
            body: `{
                "uuid": "$uuid$",
                "status": "$status$"
            }`
        }
    });
}
DataCall('getMacrosByTypeAndTag',`
    select [Name], [TextNo] from Macro where [Type] = $type$ and Tag = '$tag$'
`);
DataCall('getMacrosByTag',`
    select Tag from Macro where [Type] = $type$ group by Tag order by Tag asc
`);
DataCall('getMemoText',`
    declare @pMemoNo int = $memoNo$, @result varchar(max)
    exec @result = dbo.GetMemoText @pMemoNo
    select @result [Result]
`);
DataCall('putMemoText',`
    declare @pMemoNo int, @pText varchar(max), @pResult int
    set @pMemoNo = (SELECT TextNo FROM Macro WHERE [Name] = 'ConvertedTest' AND [Type] = 3 AND Tag = 'Web Wizard');
    set @pText = '$text$'
    exec PutMemoText @pMemoNo, @pText, @pResult out
    select @pResult [Result]
`);
WebDataCall('updateWizardStatus',`
    ${environmentContext.Url.BusinessLayer}/utUpdateWizardStatus
    `);
//#endregion
//#region Page Declaration
const page1 = new Wizard.Page({ title: 'Convert', description: 'Convert JS to WW' });
app.addPage(page1);
//#endregion
//#region Page 1
page1.addControl(new Wizard.Control({id: 'p1MacroType', type: 'dropdown', displayKey: 'description', widthPercentage: "33", text: 'Type', onChange: 'p1OnChangeType()'}));
page1.addControl(new Wizard.Control({id: 'p1TagList', type: 'dropdown', displayKey: 'Tag', widthPercentage: "33", text: 'Tags', onChange: 'p1OnChangeTagList()'}));
page1.addControl(new Wizard.Control({id: 'p1MacroList', type: 'dropdown', displayKey: 'Name', widthPercentage: "33", text: 'Macro\'s', onChange: 'p1OnChangeMacroList()'}));
page1.addControl(new Wizard.Control({id: 'p1Convert', type: 'button', text: 'Convert', primary: 'true', onClick: 'p1OnClickConvert()'}));
//#endregion
//#region Script
app.script = () => {
    //#region Classes    
    class ActiveXObject {
        constructor (pProgID) {
            this.progid = pProgID;
            this.obj = [];
            this.pages = [];
        }
        AddEventObject (pName, pObj) {
            this.obj.push ({'name': pName, 'obj': pObj});
        }
        AddPage (pID, pTitle, pDescription) {
            let p = new AXEPage(pID, pTitle, pDescription);
            this.pages.push (p);
            return p;
        }
        SetSize (pL, pW) {

        }
        AddCoverPage (pID, pName, pDescription, pBody) {
            //let p = new AXECoverPage(pID,pName,pDescription,pBody);
            //this.pages.push(p);
            //return p;
        }
        Close(){

        }
        Start(){

        }
        GetReturnCode(){
            return 1;
        }
        Popup(pMsg, pSec, pHeader, pWidth){

        }
        Copyfile(pPath){

        }
    }
    class AXEPage {
        
        constructor (pID, pTitle, pDescription) {
            this.id = pID;
            this.title = pTitle;
            this.description = pDescription;
            this.fields = [];
            this.items = [];
            this.values = [];
            this.sizes = [];
            this.gridLayout = [];
            this.eventScripts = [];
            this.enabled = [];
            this.margins = [];
        }        
        AddField(pID, pName, pType, pProperties){
            this.fields.push(new AXEFields(pID, pName, pType, pProperties));
        }
        SetGridColumns(pC){
            this.columns = pC;
        }
        SetGridRows(pR){
            this.rows = pR;
        }
        SetItems(pField, pValue){
            this.items.push(new AXEFieldsItems(pField,pValue));
        }
        SetSize(pField, pWidth, pHeight){
            this.sizes.push(new AXEFieldsSizes(pField, pWidth, pHeight));
        }
        SetGridLayout(pField, pRow, pCol, pWidth, pHeight){
            this.gridLayout.push(new AXEGirdLayout(pField, pRow, pCol, pWidth, pHeight));
        }
        SetEventScript(pField, pEvent, pFunction){
            this.eventScripts.push(new AXEEventScript(pField,pEvent,pFunction));
        }
        SetValue(pField, pValue){
            this.values.push(new AXEFieldsValues(pField,pValue));
        }
        Setvalue(pField, pValue){
            this.SetValue(pField,pValue);
        }
        SetEnabled(pField, pValue){
            this.enabled.push(new AXEFieldsEnabled(pField, pValue));
        }
        SetMargin(pField, pTop, pRight, pBottom, pLeft){
            this.margins.push(new AXEFieldsMargin(pField, pTop, pRight, pBottom, pLeft));
        }
        getValue(pField){
            let value = null;
            this.values.forEach(x=>{
                if(x.field == pField){
                    value = x.value;
                }
            });
            if(value == null || typeof(value) == "undefined"){
                value = "value1|value2;value1|value2;value1|value2;"
            }
            return value;
        }
        GetValue(pField){
            return this.getValue(pField);
        }
        ClearGridRows(pGrid){

        }
        BlankNextPage(){

        }
        AddGridRow(pField, pValues){

        }
        GetSelectedRows(pField){
            return `value1|value2`;
        }
        toWWString(i = 0){
            return `const page${i} = new Wizard.Page({ title: ''${this.title}'', description: ''${this.description}''});\napp.addPage(page${i});\n`;//, onNext: ''p${i}_onNext()''
        }
    }
    class AXECoverPage{
        constructor(pID, pName, pDescription, pBody){
            this.id = pID;
            this.name = pName;
            this.description = pDescription;
            this.body = pBody;
        }
    }
    class AXEFields{
        constructor(pID, pName, pType, pProperties){
            this.id = pID;
            this.name = pName;
            this.type = pType;
            this.properties = pProperties;
        }
        toWWString(i = 0, width){
            return `page${i}.addControl(new Wizard.${this.checkControl(this.type)}({ id: ''${'p' + i + '_' + this.id}'', type: ''${this.convertType(this.type)}'', text: ''${this.validateName(this.name,this.type,this.properties)}'', widthPercentage: ''${width}'', onChange: ''p${i}_onChange${this.id}()'' ${this.validateHeader(this.type, this.properties)}}));\n`;
        }
        checkRequired(i){
            let allProperties = this.properties.split(";");
            for (let x = 0; x < allProperties.length; x++) {
                let valuePair = allProperties[x].split("=");
                if(valuePair[0] == "required"){
                    return valuePair[1].toLowerCase();
                }
            }
            return ``;
        }
        convertType(value){
            // ! OLD
            // combo, text, Grid, date, button, file, check
            // * NEW 
            // button, text, number, file, checkbox, label, date, dropdown, radio, textarea, Table
            value = value.toLowerCase();
            switch(value){
                case 'combo':
                    return 'dropdown';
                case 'text':
                    return value;
                case 'grid':
                    return 'table';
                case 'date':
                    return value;
                case 'spin':
                    return 'number';
                case 'button':
                    return value;
                case 'file':
                    return value;
                case 'check':
                    return 'checkbox';
            }
        }
        validateName(name, type, properties){
            type = type.toLowerCase();
            if(type == "button"){
                let fS = properties.split(";");
                fS.forEach(x=>{
                    let sS = x.split("=");
                    if(sS[0].toLowerCase() === "value"){
                        name = sS[1].toString();
                    }
                });
            }else{
                return name;
            }
            return name;
        }
        validateHeader(type, properties){
            if(this.convertType(type.toLowerCase()) == "table"){
                let data = `, headers: [`;
                let allProps = [];
                let allProperties = properties.split(";");
                for (let x = 0; x < allProperties.length; x++) {
                    if(allProperties[x].includes("columns")){
                        let allFields = allProperties[x].split("=");
                        let allColumns = allFields[1].split("|");
                        for (let y = 0; y < allColumns.length; y++) {
                            let value = allColumns[y].substring(0, (allColumns[y].indexOf("(")));
                            data += `{ key: "${value}", caption: "${value.toUpperCase()}" },`
                            allProps.push({ key : value, caption: value.toUpperCase()})
                        }
                    }
                }
                data += `]`;
                return data;
            }else{
                return ``;
            }
//             , headers: [{ key: 'projectCode', caption: 'Project Code' }, { key: 'invoiceNo', caption: 'Invoice No' }, { key: 'endDate', caption: 'End Date' },
// { key: 'days', caption: 'Days' }, { key: 'invAmount', caption: 'Inv Amount' }, { key: 'interest', caption: 'Interest' }, { key: 'totalAdminFee', caption: 'Total Admin Fee' }, 
// { key: 'tons', caption: 'Tons' },{ key: 'valueIncVAT', caption: 'Value Inc VAT' }] 
        }
        checkControl(type){
            if(this.convertType(type.toLowerCase()) == "table"){
                return `Table`;
            }else{
                return `Control`;
            }
        }
    }
    class AXEFieldsItems{
        constructor(pField, pValue){
            this.field = pField;
            this.value = pValue;
        }
        toWWString(i = 0){
            return `$scope.p` + i + `_` + this.field + `.options = [\"${this.splitString()}\"];\n`
        }
        splitString(){
            return this.value.split("|").join("\",\"");
        }
    }
    class AXEFieldsValues{
        constructor(pField, pValue){
            this.field = pField;
            this.value = pValue;
        }
    }
    class AXEFieldsSizes{
        constructor(pField, pWidth, pHeight){
            this.field = pField;
            this.width = pWidth;
            this.height = pHeight;
        }
    }
    class AXEFieldsEnabled{
        constructor(pField, pValue){
            this.field = pField;
            this.value = pValue;
        }
    }
    class AXEGirdLayout{
        constructor(pField, pRow, pCol, pWidth, pHeight){
            this.field = pField;
            this.row = pRow;
            this.col = pCol;
            this.width = pWidth;
            this.height = pHeight;
        }
    }
    class AXEEventScript{
        constructor(pField, pEvent, pFunction){
            this.field = pField;
            this.event = pEvent;
            this.function = pFunction;
        }
    }
    class AXEFieldsMargin{
        constructor(pField, pTop, pRight, pBottom, pLeft){
            this.field = pField;
            this.top = pTop;
            this.right = pRight;
            this.bottom = pBottom;
            this.left = pLeft;
        }
    }
    class PerformanceMetrics{
        constructor(pConn){
            this.conn = pConn;
        }
        AddActivity(pName, pDescription, pCategory, pChildRef, pCode){
            this.name = pName;
            this.description = pDescription;
            this.category = pCategory;
            this.childref = pChildRef;
            this.code = pCode;
        }
    }
    class SQLHandler{
        constructor(pConn){
            this.conn = new Connection(pConn);
            this.queries = [];
        }
        GetScalar(pQuery){
            this.queries.push(pQuery);
        }
        Fill(pQuery){
            this.queries.push(pQuery);
            let sqlFill = {
                Row : [1,2,3]
            }            
            return sqlFill;
        }
        SQLDate(pDate, pFormat){
            let date = new Date(pDate);
            return date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate();
        }
    }
    class Connection{
        constructor(pConn){
            this.conn = pConn;
            this.queries = [];
        }
        Execute(pQuery){
            this.queries.push(pQuery);
        }
    }
    class Vat_Object{
        constructor(pSql){
            this.sql = pSql;
        }
    }
    class StorageSchemaObject{
        constructor(pSql){
            this.sql = pSql;
        }
    }
    class INIClass{
        constructor(){

        }
    }
    class DateUtility{
        constructor(){

        }
    }       
    //#endregion
    //#region Global Values
    // * Values required in eval script
    var Conn = "This connection";
    var offsetLine = "value";
    var invoiceID = "value";
    var Userid = "007";
    var UserID = "007";
    var CofcoPage = "value";
    var StandardPage = "value";
    var Dyna_Filepath = "value";
    var ChildInstanceRefNo = "10501";
    var StoreResult = "value";
    var PFECalcTrigger = "value";
    var readXML = "value";
    var PFECalcRespons = "value";
    var CreatePFECalcDoc = "value";
    var DeleteDocs = "value";

    // * Global Values
    let productionLevel = false;
    let GlobalData = {
        layoutWidth: 100
    };
    //#endregion
    //#region Set Fields
    $scope.p1MacroType.options = [
        {description: "Global", key: 1},
        {description: "Macro", key: 2},
        {description: "Web Wizard", key: 3}
    ];
    //#endregion
    //#region Functions
    function RenameVariables(){
        GlobalData.memoText = GlobalData.memoText.split("\n");
        for (let x = 0; x < GlobalData.memoText.length; x++) {
            if(GlobalData.memoText[x].includes("new ActiveXObject(\"AXE.Wizard\");")){
                GlobalData.memoText[x] = "var wiz = new ActiveXObject(\"AXE.Wizard\");";
            }
            if(GlobalData.memoText[x].includes("new SQLHandler(Conn)")){
                GlobalData.memoText[x] = "var sql = new SQLHandler(Conn);";
            }            
        }
        GlobalData.memoText = GlobalData.memoText.join("\n");
    }
    function BuildNewWebWizard(wiz){
        cntx = 'Wizard object';
        let s = header();
        try {            
            cntx = 'Pages';
            let scriptValues = "";
            let i = 0;
            let events = [];
            //#region Pages
            wiz.pages.forEach(p => {
                s += `//#region page${i}\n`
                //scriptValues += `$scope.p${i}_onNect = async function(){\nlet allowPage = true\n\n`
                s += p.toWWString(i);
                p.gridLayout.sort((a, b) => (a.row > b.row) ? 1 : -1);  
                for (let y = 0; y < p.gridLayout.length; y++){
                    for (let x = 0; x < p.fields.length; x++) {
                        let widthObject = {};
                        if(p.gridLayout[y].field == p.fields[x].id && y == 0){
                            widthObject = calculateWidth(i, p.fields[x], p, p.gridLayout[y]);  
                            s += widthObject.body;
                            s += p.fields[x].toWWString(i, widthObject.width);
                        }else if(p.gridLayout[y].field == p.fields[x].id && y != 0){
                            widthObject = calculateWidth(i, p.fields[x], p, p.gridLayout[y], p.gridLayout[y - 1]);
                            s += widthObject.body;
                            s += p.fields[x].toWWString(i, widthObject.width);
                        }
                        // if(p.checkRequired()){
                        //     scriptValues += pageCheck(p, i);
                        //     events.push(generateEvent(p, i));
                        // }
                        
                    }
                }
                //scriptValues += `\nif(allowPage){$scope.page = ${i + 1}}\n}\n`;
                p.items.forEach(x=>{
                    scriptValues += x.toWWString(i);
                });
                i++;
                s += `//#endregion\n\n`
            });
            scriptValues += addEvents();
            //#endregion

            //#region Objects
            cntx = 'Data Calls';
            // wiz.obj.forEach(o => {
            //     if(typeof(o.obj) == "function"){
            //         o.obj();
            //     }
            // });
            wiz.pages.forEach(x => {
                if(typeof(x.EnterSript) == "function"){
                    x.EnterSript();
                }
                if(typeof(x.ExitScript) == "function"){
                    x.ExitScript();
                }
                x.eventScripts.forEach(y => {
                    if(typeof(y.function) == "function"){
                        y.function();
                    }
                });
            });

            //#endregion
            
            s += `\napp.script = () => {\n\n`
            s += scriptValues;
            s += `\n};\napp.toHTMLFile();`
        }
        catch (ex) {
            console.log (cntx, ex.message);
            s = '*** error ***';
        }
        return s;
    }
    function generateEvent(page, i){
        return `\n$scope.p${i}_onChange${page.id} = async function(){
            if($scope.p${i}_${page.id}.value != null){
                $scope.p${i}_${page.id}.error = null;
            }
        }\n`        
    }
    function pageCheck(page, i){
        return `\nif($scope.p${i}_${page.id}.value == null){
            $scope.p${i}_${page.id}.error = "This fields is required";
            allowPage = false;
        }\n`
    }
    function calculateWidth(i, fields, page, layout, prevLayout){
        if(fields.convertType(fields.type.toLowerCase()) == "table" || fields.convertType(fields.type.toLowerCase()) == "file"){
            let value = {
                width: 100,
                body: ``
            }
            GlobalData.layoutWidth = 100;
            return value;
        }else{
            if(typeof(prevLayout) == "undefined"){
                let totalWidth = 4;
                let fieldWidth = layout.width;
                let value = {
                    width: parseInt(Math.round(((fieldWidth/totalWidth) * 100))),
                    body: ""
                }
                GlobalData.layoutWidth = GlobalData.layoutWidth - value.width;
                return value;
            }else{
                let totalWidth = 4;
                let currentFieldWidth = layout.width;
                let currentRow = layout.row;
                let prevRow = prevLayout.row;
                if(currentRow == prevRow){
                    let value = {
                        width: parseInt(Math.round(((currentFieldWidth/totalWidth) * 100))),
                        body: ""
                    }                    
                    return value;
                }else{
                    let value = {
                        width: parseInt(Math.round(((currentFieldWidth/totalWidth) * 100))),
                        body: `page${i}.addControl(new Wizard.Control({ id: ''p${i}_spacer'', type: ''label'', text: '''', widthPercentage: ''${GlobalData.layoutWidth}''}));\n`
                    }
                    GlobalData.layoutWidth = 100;
                    return value;
                }
            }
        }        
    }
    function header(){
        return `const Wizard = require(''./web-wizard/classes/wizard.js'');\nconst app = new Wizard.App({ wwWizardData: wwWizardData, webServiceUrl: ''\${environmentContext.Url.BusinessLayer}'', primaryColor: ''rgb(223, 39, 39)'' });\n\nfunction WebDataCall(id, url){
            app.addDataCall({
                type: ''web'', data: {
                    id: id,
                    method: ''post'',
                    url: url,
                    body: \`{
                        "uuid": "$uuid$",
                        "status": "$status$"
                    }\`
                }
            });
        }\n\nWebDataCall(''updateWizardStatus'',
        \`\${environmentContext.Url.BusinessLayer}\/utUpdateWizardStatus\`
    );\n\n`
    }    
    function addEvents(){
        return `$scope.appCancel = async () => {            
            let updateWizardStatusResponse = await wwDoDataCall({
                id: ''updateWizardStatus'', parameters: {
                    uuid: wwUuid,
                    status: ''Finished''
                }
            });
        }`
    }
    function preParse (pScript) {
        let ret = {script: null, added: [], errors: [], keep: []};
        let cnt = 0;
        while (cnt++ < 100) {
            // Eval
            let msg = null;
            try {
                eval (`function JSMsg (msg, caption) { ret.keep.push(msg); }; \nfunction GetNewNoteID() { return 1; }\n ${pScript}`);
            }
            catch (err) {
                msg = err.message;
            }

            if(ret.keep.length != 0){
                msg = ret.keep[0];
                ret.keep = [];
            }
            // Test if successful
            if (!msg) break;
            // Search for solution
            while (true) {
                // Scenario 1 - object is not defined
                let s = msg.split (' ');
                //console.log('object', s);
                if ((s.length == 4 && s[1] == 'is' && s[2] == 'not' && s[3] == 'defined') || (s.length == 5 && s[1] == 'is' && s[2] == 'not' && s[3] == "a" && s[4] == 'function')) {
                    // Add object to script to eval again
                    pScript = `let ${s[0]} = function () {};\r\n` + pScript;
                    ret.added.push (s[0]);
                    break;
                }
                // Scenario 2 - next error type
                // DeleteReserveDebtors is not a function
                // if (s.length == 5 && s[1] == 'is' && s[2] == 'not' && s[3] == "a" && s[4] == 'function') {
                //     // Add object to script to eval again
                //     console.log('Array',s);
                //     pScript = `function ${s[0]}() {};\r\n` + pScript;
                //     ret.added.push (s[0]);
                //     break;
                // }
                // test and do something
                break;
                // Scenario 3 - next error type
                // test and do something
                break;
                // Default - unhandled error
                res.errors.push (msg);
                break;
            }
        }
        if (cnt >= 100) res.errors.push ('More than 100 errors encountered');
        // Return
        pScript = `//\n//PREPARSED ${ret.added.length != 0 || ret.errors.length != 0 ? '(some changes made or errors encountered, attention needed)' : ''}\n//\n\n${pScript}`;
        ret.script = pScript;
        return ret;
    }
    
    async function StartConversion(){
        let res = null;
        try {
            await RenameVariables();
            $scope.JSMsgErr = [];
            let value = preParse(GlobalData.memoText);
            //console.log('Script', value.script);
            console.log('Value', value);
            eval(value.script);
            console.log('wiz', wiz);
            let builtDoc = await BuildNewWebWizard(wiz);
            const response = await wwDoDataCall({ id: 'putMemoText', parameters: { text: builtDoc.toString() } });
            const valPutValue = JSON.parse(response);
            console.log('valPutValue',valPutValue);
        } catch (error) {
            res = error.message;
        }
        return res;
    }
    function JSMsg(value){
        console.log('JSMsg:', value);
    }
    function GetNewNoteID(){
        return 1;
    }
    //#endregion
    //#region Events
    $scope.p1OnChangeType = async function(){
        if($scope.p1MacroType.value != null){
            const responseByTag = await wwDoDataCall({ id: 'getMacrosByTag', parameters: { type: JSON.parse($scope.p1MacroType.value).key } });
            const macrosByTag = JSON.parse(responseByTag);
            $scope.p1TagList.options = macrosByTag;
            $scope.$apply();
        }
    }
    $scope.p1OnChangeTagList = async function(){
        if($scope.p1TagList.value != null){
            const responseByTagAndType = await wwDoDataCall({ id: 'getMacrosByTypeAndTag', 
            parameters: { 
                type: JSON.parse($scope.p1MacroType.value).key,
                tag: JSON.parse($scope.p1TagList.value).Tag} });
            const macrosByTagAndType = JSON.parse(responseByTagAndType);
            $scope.p1MacroList.options = macrosByTagAndType;
            $scope.$apply();
        }
    }
    $scope.p1OnClickConvert = async function(){
        const responseMemo = await wwDoDataCall({ id: 'getMemoText', parameters: { memoNo: JSON.parse($scope.p1MacroList.value).TextNo } });
        const memoData = JSON.parse(responseMemo);
        GlobalData.memoText = memoData[0].Result;
        let res = await StartConversion();
        if(res != null){
            //Unsuccessful
            $scope.wwPopup = {
                visible: true,
                closeOutsideClick: false,
                header: 'Unsuccessful',
                body: `Reason: ${res}.`,
                buttons: [
                    {
                        function: $scope.popupFunctionOK,
                        text: 'OK',
                    }
                ]
            }
        }else{
            //Successful
            $scope.wwPopup = {
                visible: true,
                closeOutsideClick: false,
                header: 'Successful',
                body: 'Script converted.',
                buttons: [
                    {
                        function: $scope.popupFunctionOK,
                        text: 'OK',
                    }
                ]
            }
        }
    }
    $scope.popupFunctionOK = function(){
        $scope.wwPopup.visible = false;
    }
    $scope.appCancel = async () => {
        let updateWizardStatusResponse = await wwDoDataCall({
            id: 'updateWizardStatus', parameters: {
                uuid: wwUuid,
                status: 'Finished'
            }
        });
        console.log(updateWizardStatusResponse);
    }
    //#endregion
};
//#endregion
//#region Create File
app.toHTMLFile();
//#endregion