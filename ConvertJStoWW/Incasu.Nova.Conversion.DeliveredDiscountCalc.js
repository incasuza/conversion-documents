//New Delivered Stock Calc Initiate with PFE

//$include (Class_SQLHandler)
//$include (Schema Class)
//$include(Ini_Class)
//$include (GetNextNumber)
//$include (PM) 
//$include (Vat_Object)
//$include (DateUtility)
//$include (JSON)
//$include (PFE_Generate_Json)
//$include (GetNextNoteID)
//$include (FinancePFE)

//2019-11-25 BuildXML rollback

try {
    cntx = "Initialize";
    var pm = new PerformanceMetrics(Conn);

    var sql = new SQLHandler(Conn);
    var vat_obj = new Vat_Object(sql);
    var wiz = new ActiveXObject("AXE.Wizard");
    var so = new StorageSchemaObject(sql);
    var ini = new INIClass();
    var offsetLine = new Object(offsetLine);

    var invoiceID = new Object(invoiceID)
    var Userid = new Object(Userid)
    var CofcoPage = new Object(CofcoPage)
    var StandardPage = new Object(StandardPage)
    var dUtil = new DateUtility();
    Userid.Userid = UserID
    var ReserveID = new Object();
    var DynaFilepath = new Object();
    CofcoPage.Value = 1
    StandardPage.Value = 1
    DynaFilepath.value = Dyna_Filepath.toString();
    var ManualCalc = new Object();
    ManualCalc.value = false;
    var GlobalVar = new Object();

    wiz.SetSize(1100, 750);
    wiz.AddEventObject("sql", sql);
    wiz.AddEventObject("GlobalVar", GlobalVar);
    wiz.AddEventObject("trim", trim);
    wiz.AddEventObject("Popup", Popup);
    wiz.AddEventObject("so", so);
    wiz.AddEventObject("GetPaymentAccount", GetPaymentAccount);
    wiz.AddEventObject("ChildInstanceRefNo", ChildInstanceRefNo);
    wiz.AddEventObject("ini", ini);
    wiz.AddEventObject("BuildXML", BuildXML);
    wiz.AddEventObject("GetDiscountCalcs", GetDiscountCalcs);
    wiz.AddEventObject("Userid", Userid);
    wiz.AddEventObject("CofcoPage", CofcoPage);
    wiz.AddEventObject("StandardPage", StandardPage);
    wiz.AddEventObject("vat_obj", vat_obj);
    wiz.AddEventObject("dUtil", dUtil);
    wiz.AddEventObject("ReserveDebtors", ReserveDebtors);
    wiz.AddEventObject("BuildChangeXML", BuildChangeXML);
    wiz.AddEventObject("StoreResult", StoreResult);
    wiz.AddEventObject("PFECalcTrigger", PFECalcTrigger);
    wiz.AddEventObject("readXML", readXML);
    wiz.AddEventObject("ReserveID", ReserveID);
    wiz.AddEventObject("PFECalcRespons", PFECalcRespons);
    wiz.AddEventObject("DynaFilepath", DynaFilepath);
    wiz.AddEventObject("GetNewNoteID", GetNewNoteID);
    wiz.AddEventObject("CreateDoc", CreateDoc);
    wiz.AddEventObject("pm", pm);
    wiz.AddEventObject("DeleteReserveDebtors", DeleteReserveDebtors);
    wiz.AddEventObject("CreatePFECalcDoc", CreatePFECalcDoc);
    wiz.AddEventObject("ManualCalc", ManualCalc);
    wiz.AddEventObject("UploadDoc", UploadDoc);
    wiz.AddEventObject("UploadDocData", UploadDocData);
    wiz.AddEventObject("DeleteDocs", DeleteDocs);
    wiz.AddEventObject("AddFSASummary", AddFSASummary);
    wiz.AddEventObject("FSANumber", FSANumber);
    wiz.AddEventObject("AddAddressDetails", AddAddressDetails);

    //

    var cover = wiz.AddCoverPage("cover", "Delivered Stock Calculation Initiation", "Prepare Delivered Stock Calculation Initiation", "* Enter the correct criteria");
    //--------------------------------------------

    var p0 = wiz.AddPage("p0", "Delivered Stock Discount Calc Options", "Choose The Correct Option");
    p0.SetGridColumns(4);
    p0.SetGridRows(5);

    p0.AddField("Action", "Discount Calc Action", "combo", "default=CREATE NEW");
    p0.AddField("dProject", "Project Code", "text", "readonly=true;default=" + ChildInstanceRefNo)
    p0.AddField("DiscCalcs", "Choose Calc To Amend", "Grid", "columns=State(type:string,width:180)|DataNo(type:string,width:80)|ProjectCode(type:string,width:80)|Client(type:string,width:250)|TransType(type:string,width:250)|Preparer(type:string,width:Fill);enabledelete=false;readonly=true");
    p0.AddField("DeleteCalc", "", "button", "value=Delete Discount Calculation");
    p0.AddField("invoice", "Invoice for Commodity Supply", "check", "default=false")

    p0.SetItems("Action", "Create New|Amend Existing|Finalize".toUpperCase());

    p0.SetSize("DiscCalcs", 910, 450);

    p0.SetGridLayout("Action", 0, 0, 1, 1);
    p0.SetGridLayout("dProject", 0, 1, 1, 1);
    p0.SetGridLayout("invoice", 0, 2, 1, 1);
    p0.SetGridLayout("DiscCalcs", 1, 0, 1, 4);

    p0.SetGridLayout("DeleteCalc", 2, 0, 1, 1);
    p0.SetEventScript("DeleteCalc", "change", DeleteCalculation)

    p0.EnterScript = GetDiscountCalcs;
    p0.ExitScript = GetCalcDetails;
    //--------------------------------------------	
    var p1 = wiz.AddPage("p1", "Delivered Stock Discount Calc Details", "Edit Discount Calc Details");
    p1.SetGridColumns(3);
    p1.SetGridRows(9);

    p1.AddField("ClientName", "Client Name:", "text", "readonly=true");
    p1.AddField("CalcTpe", "Transaction type:", "combo", "required=true");
    p1.AddField("TransDate", "Start Date:", "date", "required=false");

    var projRate = sql.GetScalar("Select Top 1 cast ([Value] * 100 as decimal(10,2)) as \'Rate\' From Rate Where RateCode = (Select RateCode From GlAccount where GlAccCode = \'975\' and ProjectCode =" + ChildInstanceRefNo + ") and ProjectCode = 0 order by DT_EffectiveDate desc");
    p1.AddField("ActualRate", "Actual Interest Rate:", "spin", "default=" + projRate + ";required=true;min=0.00;max=999.00;increment=0.50;trailing=%");
    p1.AddField("AdminFee", "Admin Fee Per Unit Payable At Buy Back:", "spin", "required=false;min=0.00;max=999.00;increment=1.00;leading=R");
    p1.AddField("Vat", "Vat:", "text", "readonly=true;default=0");
    p1.AddField("PayTol", "Payment tollerance(DAYS):", "spin", "default=0;required=false;min=0.00;max=999.00;increment=1.00;trailing=");
    p1.AddField("TypeComm", "Commodity Type:", "Combo", "required=false");
    p1.AddField("Statement", "Create Statement", "Combo", "default=Statement for all Stock Lines;required=true");
    p1.AddField("Payment", "Create a Payment", "Combo", "default=No Payment;required=true")

    var getAdminFee = sql.GetScalar("select ISNULL( Value,0)  from AttribValue where EntityType = \'ProjectType\' and EntityTypeNo = \'2\' and AttributeCode = \'AdminFee\' and EntityNo = \'" + ChildInstanceRefNo + "\'")
    if (getAdminFee == -1 || getAdminFee == "") {
        getAdminFee = 0;
    }

    var getComm = sql.Fill("SELECT [Description] FROM TypeCom WHERE CommodityTypeNo IN (SELECT CommodityTypeNo FROM [Contract] WHERE ContractTypeNo = 19 AND ProjectCode = \'" + ChildInstanceRefNo + "\' and [Status] = 1)");
    var Commodity = "";
    if (getComm != -1) {
        for (a = 0; a < getComm.Row.length; a++) {
            if (a + 1 != getComm.Row.length)
                Commodity += getComm.Row[a].Description + "|"
            else
                Commodity += getComm.Row[a].Description
        }
    }
    else {
        Commodity = "No Commodity Set for project " + ChildInstanceRefNo
    }

    var today = sql.Fill("Select GetDate() [today]")
    today = new Date(today.Row[0].today);
    today = sql.SQLDate(today, 2);

    p1.Setvalue("TransDate", today)
    p1.SetItems("CalcTpe", "Standard Delivered Calculation|COFCO Calculation")
    p1.SetItems("Statement", "Statement for all Stock Lines|Statement per Stock Line|No Statement")
    p1.SetItems("Payment", "Link Payment|STP Transfer Payment|No Payment")
    p1.SetValue("AdminFee", getAdminFee)
    p1.SetItems("TypeComm", Commodity.toUpperCase())

    p1.SetEventScript("TypeComm", "Change", TypeCommSet)

    p1.SetGridLayout("TransDate", 0, 1, 1, 1);
    p1.SetGridLayout("CalcTpe", 1, 0, 1, 1);
    p1.SetGridLayout("AdminFee", 2, 0, 1, 1);
    p1.SetGridLayout("ActualRate", 2, 1, 1, 1);
    p1.SetGridLayout("TypeComm", 3, 0, 1, 1);
    p1.SetGridLayout("Vat", 4, 0, 1, 1);
    p1.SetGridLayout("PayTol", 4, 1, 1, 1);
    p1.SetGridLayout("Payment", 5, 0, 1, 1);
    p1.SetGridLayout("Statement", 6, 0, 1, 1);


    p1.ExitScript = NextPage;

    p1.SetEventScript("CalcTpe", "change", CalcTpe)
    p1.SetEventScript("TransDate", "change", CommVat)

    p1.SetEnabled("Statement", false)
    p1.SetEnabled("Payment", false)
    //--------------------------------------------	
    var pBankAcc = wiz.AddPage("pBankAcc", "Choose Payment Bank Account ", "")
    pBankAcc.SetGridColumns(5);

    //AW Added
    pBankAcc.AddField("ReloadFilter", "", "button", "value=Filter");
    pBankAcc.AddField("BeneficiaryFilter", "Beneficiary Filter", "text", null);

    pBankAcc.AddField("BankAccount", "Payment Bank To Details", "Grid", "columns=Account No(type:string,width:fill)|Payee(type:string,width:fill)|Bank(type:string,width:250)|Region(type:string,width:fill);enabledelete=false;readonly=true;multiselect=false");
    pBankAcc.AddField("Reference", "Reference", "text", null);

    pBankAcc.SetSize("BankAccount", 910, 400);
    //AW Added
    pBankAcc.SetGridLayout("BeneficiaryFilter", 0, 0, 1, 1);
    pBankAcc.SetGridLayout("ReloadFilter", 0, 1, 1, 1);

    pBankAcc.SetGridLayout("BankAccount", 1, 0, 1, 5);
    pBankAcc.SetGridLayout("Reference", 2, 0, 1, 1);

    //AW Added
    pBankAcc.SetEventScript("ReloadFilter", "Change", GetPaymentAccount);
    //pBankAcc.SetEventScript("Reload", "Change", ReloadAccounts)
    pBankAcc.ExitScript = pBankAcc_Exit
    //--------------------------------------------	

    var SdtCalc = wiz.AddPage("SdtCalc", "Delivered Stock Dicount Calc Details", "Insert stock lines");
    SdtCalc.SetGridColumns(3);
    SdtCalc.SetGridRows(7);

    SdtCalc.AddField("stockFile", "Stock File:", "file", "required=false;filter=Excel Files|*.xlsx");
    SdtCalc.AddField("CreateTemp", "", "button", "value=Create Import Template");
    SdtCalc.AddField("EndDate", "End Date*:", "date", "required=false");
    SdtCalc.AddField("InvoiceNo", "Invoice number*:", "text", "readonly=false");
    SdtCalc.AddField("Ton", "Units*:", "text", "required=false;default=0");
    //AW Added
    SdtCalc.AddField("Measurement", "Select Unit of Measure:", "Combo", "items=Tonnage|Kilogram|Gram|KiloLitre|Litre|MilliLitre;required=true;default=0");
    SdtCalc.AddField("InvAmount", "Inv Amount", "text", "required=false;default=0")
    SdtCalc.AddField("DaysFinanced", "Days Financed", "text", "required=false;default=0;readonly=true")
    SdtCalc.AddField("AddBut", "", "button", "value=Add Stock Line");
    SdtCalc.AddField("DeleteBut", "", "button", "value=Delete All Stock Lines");
    SdtCalc.AddField("EditBut", "", "button", "value=Edit Stock Line");
    SdtCalc.AddField("FSANo", "FSA Contract Number:", "combo", "required=true");
    //AW Check here, will have to add the unit ofmeasurement to the gird too.
    SdtCalc.AddField("StockDet", "Stock Details", "Grid", "columns=Project Code(type:string,width:80,readonly:true)|Invoice No(type:string,width:80,readonly:true)|End Date(type:string,width:80,readonly:true)|Days(type:string,width:100,readonly:true)|Inv Amount(type:string,width:80,readonly:true)|Interest(type:string,width:100,readonly:true)"
        + "|Total Admin Fee(type:string,width:120,readonly:true)|Tons(type:string,width:80,readonly:true)|Value Incl VAT(type:string,width:120,readonly:true)|Amount Excl VAT(type:String,width:120,readonly:true)|VAT Amount(type:string,width:80,readonly:true)|FSA Number(type:String,width:120,readonly:true)|Measurement(type:string,width:100,readonly:true);enabledelete=true");

    SdtCalc.SetGridLayout("stockFile", 0, 0, 2, 2);
    SdtCalc.SetGridLayout("CreateTemp", 0, 2, 2, 2);
    SdtCalc.SetGridLayout("FSANo", 2, 0, 1, 1);
    SdtCalc.SetGridLayout("InvoiceNo", 2, 1, 1, 1);
    SdtCalc.SetGridLayout("InvAmount", 2, 2, 1, 1);
    SdtCalc.SetGridLayout("Measurement", 3, 0, 1, 1);
    SdtCalc.SetGridLayout("Ton", 3, 1, 1, 1);
    SdtCalc.SetGridLayout("EndDate", 4, 0, 1, 1);
    SdtCalc.SetGridLayout("DaysFinanced", 4, 1, 1, 1);
    SdtCalc.SetGridLayout("AddBut", 5, 0, 1, 1);
    SdtCalc.SetGridLayout("DeleteBut", 5, 1, 1, 1);
    SdtCalc.SetGridLayout("EditBut", 5, 2, 1, 1);
    SdtCalc.SetGridLayout("StockDet", 7, 0, 1, 3);

    SdtCalc.SetSize("AddBut", 150, 60);
    SdtCalc.SetSize("DeleteBut", 150, 60);
    SdtCalc.SetSize("EditBut", 150, 60);
    SdtCalc.SetSize("StockDet", 1000, 300);
    SdtCalc.SetSize("CreateTemp", 150, 48);
    SdtCalc.SetMargin("CreateTemp", -150, 0, 0, 0)

    SdtCalc.SetEventScript("EndDate", "change", daysEvent);
    SdtCalc.SetEventScript("FSANo", "change", FSAChange);
    SdtCalc.SetEventScript("AddBut", "change", addStockLines);
    SdtCalc.SetEventScript("EditBut", "Change", GetEditDetail);
    SdtCalc.SetEventScript("DeleteBut", "Change", DeleteStockLines);
    SdtCalc.SetEventScript("stockFile", "Change", ImportStock);
    SdtCalc.SetEventScript("CreateTemp", "Change", CreateImportFile);

    SdtCalc.EnterScript = AmendEdit;
    SdtCalcNextP = {};
    SdtCalc.ExitScript = SdtCalcNextP;

    //--------------------------------------------

    var pCOFCO = wiz.AddPage("pCOFCO", "COFCO Calculation", "COFCO");
    pCOFCO.SetGridColumns(3);
    pCOFCO.SetGridRows(7);

    pCOFCO.AddField("EndDate", "End Date*:", "date", "required=false");
    pCOFCO.AddField("InvoiceNo", "Invoice number*:", "text", "readonly=false");
    pCOFCO.AddField("Ton", "Units*:", "text", "required=false;default=0");
    //AW Added
    pCOFCO.AddField("Measurement", "Select Unit of Measure:", "Combo", "items=Tonnage|Kilogram|Gram|KiloLitre|Litre|MilliLitre;required=true;default=0");
    pCOFCO.AddField("InvAmount", "Inv Amount", "text", "required=false;default=0")
    pCOFCO.AddField("DaysFinanced", "Days Financed", "text", "required=false;default=0;readonly=true")
    pCOFCO.AddField("PriceAdjust", "Price Adjustment", "text", "required=false;default=0;readonly=false")
    pCOFCO.AddField("AddBut", "", "button", "value=Add Stock Line");
    pCOFCO.AddField("DeleteBut", "", "button", "value=Delete All Stock Lines");
    pCOFCO.AddField("EditBut", "", "button", "value=Edit Stock Line");
    pCOFCO.AddField("FSANo", "FSA Contract Number:", "combo", "required=true");
    //AW Check here, will have to add the unit ofmeasurement to the gird too.
    pCOFCO.AddField("StockDet", "Stock Details", "Grid", "columns=Invoice No(type:string,width:80,readonly:true)|End Date(type:string,width:80,readonly:true)|Days(type:string,width:100,readonly:true)|Inv Amount(type:string,width:80,readonly:true)|Price Adjustment(type:string,width:80,readonly:true)"
        + "|Amount(type:string,width:80,readonly:true)|Interest(type:string,width:100,readonly:true)|Total Admin Fee(type:string,width:120,readonly:true)|Tons(type:string,width:80,readonly:true)|Value Due To Client(type:string,width:120,readonly:true)|Cheque Local(type:String,width:120,readonly:true)|FSA Number(type:String,width:120,readonly:true)|Measurement(type:string,width:100,readonly:true);enabledelete=true");

    pCOFCO.SetGridLayout("FSANo", 2, 0, 1, 1);
    pCOFCO.SetGridLayout("InvoiceNo", 2, 1, 1, 1);
    pCOFCO.SetGridLayout("InvAmount", 2, 2, 1, 1);
    pCOFCO.SetGridLayout("Measurement", 3, 0, 1, 1);
    pCOFCO.SetGridLayout("Ton", 3, 1, 1, 1);
    pCOFCO.SetGridLayout("PriceAdjust", 3, 2, 1, 1);
    pCOFCO.SetGridLayout("EndDate", 4, 0, 1, 1);
    pCOFCO.SetGridLayout("DaysFinanced", 4, 1, 1, 1);
    pCOFCO.SetGridLayout("AddBut", 5, 0, 1, 1);
    pCOFCO.SetGridLayout("DeleteBut", 5, 1, 1, 1);
    pCOFCO.SetGridLayout("EditBut", 5, 2, 1, 1);
    pCOFCO.SetGridLayout("StockDet", 6, 0, 4, 4);

    pCOFCO.SetSize("AddBut", 150, 60);
    pCOFCO.SetSize("DeleteBut", 150, 60);
    pCOFCO.SetSize("EditBut", 150, 60);
    pCOFCO.SetSize("StockDet", 1000, 380);

    pCOFCO.SetEventScript("EndDate", "change", daysEvent);
    pCOFCO.SetEventScript("FSANo", "change", FSAChange);
    pCOFCO.SetEventScript("AddBut", "change", addStockLines);
    pCOFCO.SetEventScript("EditBut", "Change", GetEditDetail);
    pCOFCO.SetEventScript("DeleteBut", "Change", DeleteStockLines);

    pCOFCO.ExitScript = COFCOCalcNextP
    //--------------------------------------------	
    var p3 = wiz.AddPage("p3", "Dicount Calc Totals", "Totals");
    p3.SetGridColumns(3);
    p3.SetGridRows(5);
    p3.EnterScript = LoadTotals;
    p3.ExitScript = SaveXML;

    cntx = "Page 4";
    var p4 = wiz.AddPage("p4", "Miller Exposure", "PFE Calc");

    p4.SetGridColumns(4);
    p4.SetGridRows(13);
    p4.AddField("MillName", "Miller name", "text", "readonly=true");
    p4.AddField("SDSID", "SDS ID", "text", "readonly=true");
    p4.AddField("CIF", "CIF Key", "text", "readonly=true");
    p4.AddField("Limit", "Limit", "text", "readonly=true");
    p4.AddField("CalcExp", "New Calculated Exposure (NCE)", "text", "readonly=true");
    p4.AddField("IntNCE", "Interest on NCE", "text", "readonly=true");
    p4.AddField("ExpAndInt", "Exposure plus Interest on NCE", "text", "readonly=true");
    p4.AddField("Res", "Reserved", "text", "readonly=true");
    p4.AddField("IntRes", "Interest on Reserved", "text", "readonly=true");
    p4.AddField("ResAndInt", "Reserved plus Interest on Reserved", "text", "readonly=true");
    p4.AddField("Available", "Limit Available", "text", "readonly=true");


    p4.SetGridLayout("MillName", 1, 1, 1, 1);
    p4.SetGridLayout("SDSID", 2, 1, 1, 1);
    p4.SetGridLayout("CIF", 3, 1, 1, 1);
    p4.SetGridLayout("Limit", 4, 1, 1, 1);
    p4.SetGridLayout("CalcExp", 5, 1, 1, 1);
    p4.SetGridLayout("IntNCE", 6, 1, 1, 1);
    p4.SetGridLayout("ExpAndInt", 7, 1, 1, 1);
    p4.SetGridLayout("Res", 8, 1, 1, 1);
    p4.SetGridLayout("IntRes", 9, 1, 1, 1);
    p4.SetGridLayout("ResAndInt", 10, 1, 1, 1);
    p4.SetGridLayout("Available", 11, 2, 1, 1);


    cntx = "Page 5";
    var p5 = wiz.AddPage("p5", "Uplaod Document", "Please upload file from credit approval before continuing");
    p5.SetGridColumns(3);
    p5.SetGridRows(10);
    p5.AddField("File", "Select supported document to upload", "File", "required=true");
    p5.SetGridLayout("File", 0, 0, 1, 2);
    cntx = "Run wizard";


    cntx = "Page 6";
    var p6 = wiz.AddPage("p6", "Upload Manual PFE Calc", "Please upload the manual PFE calculation file");
    p6.SetGridColumns(3);
    p6.SetGridRows(10);
    p6.AddField("PFEFile", "Select supported document to upload", "File", "required=true");
    p6.SetGridLayout("PFEFile", 0, 0, 1, 2);

    p6.ExitScript = ManaulStatus;
    p3.BlankNextPage();

    wiz.Start();



    cntx = "Execute";
    if (wiz.GetReturnCode() !== 1) {
    }
    //============================================================	
    //============================================================  

}
catch (ex) {
    JSMsg(ex.message, cntx);
    r = pm.AddActivity("Finance calculation", "Failed Delivered Stock Calculation ", "Prepared Delivered Stock Calculation", ChildInstanceRefNo, 2);

}
finally {
    wiz.Close();
}
function Popup(pHeader, pMsg) {
    var WshShell = new ActiveXObject("WScript.Shell");
    var BtnCode = WshShell.Popup(pMsg, 100, pHeader, 4 + 32);
    var strButton = -1
    switch (BtnCode) {
        case 6:
            strButton = 1
            break;
        case 7:
            strButton = 0
            break;
        case -1:
            strButton = 0
            break;
    }
    return strButton
}
function GetCalcDetails() {

    cntx = "GetCalcDetails 1";


    var discountCalcArray = p0.GetSelectedRows("DiscCalcs").split(";");
    var dataNo = discountCalcArray[0].split("|")[1];
    var checkDelGL = sql.Fill("select * from GLAccount where ProjectCode = " + p0.GetValue("dProject") + " and GLAccCode = \'848\'")
    if (checkDelGL == -1) {
        p0.AddInvalidMessage("Please link project code " + p0.GetValue("dProject") + " to GL 848.\r\n" +
            "The GL must be linked to the project before creating this Calc.")
    }
    invoiceID = dataNo;
    var getProjDes = sql.Fill("select * from ProjectA where ProjectCode =" + p0.GetValue("dProject"));
    p1.SetValue("ClientName", getProjDes.Row[0].ProjectDescription);

    if (p0.GetValue("Action") == "AMEND EXISTING") {
        if (p0.GetSelectedRows("DiscCalcs") == "") {
            p0.AddInvalidMessage("Please choose a Discount Calc from the list");
        }
        else {
            cntx = "Get the Data";
            Root = readXML(dataNo);
            ReserveID.value = Root.selectSingleNode("//Root/ReservedID").text
            cntx = "GetCalcDetails 2";

            if (Root.selectSingleNode("//Root/TransType").text == "Standard Delivered Calculation") {
                p1.SetValue("TransDate", Root.selectSingleNode("//Root/TransDate").text);
                p1.SetValue("ActualRate", Root.selectSingleNode("//Root/ActualInt").text);
                p1.SetValue("AdminFee", Root.selectSingleNode("//Root/AdminFeePTon").text);

                // GlobalVar.RootNode = Root.selectSingleNode("//Root/ContractNo").text;
                if (GlobalVar.FormName == "pCOFCO") {
                    pCOFCO.SetValue("FSANo", Root.selectSingleNode("//Root/ContractNo").text);
                } else if (GlobalVar.FormName == "SdtCalc") {
                    SdtCalc.SetValue("FSANo", Root.selectSingleNode("//Root/ContractNo").text);

                }
                p1.SetValue("Vat", Root.selectSingleNode("//Root/VatPercentage").text);
                p1.SetValue("PayTol", Root.selectSingleNode("//Root/PaymentToll").text);
                p1.SetValue("TypeComm", Root.selectSingleNode("//Root/TypeComm").text);
                p1.SetValue("CalcTpe", Root.selectSingleNode("//Root/TransType").text);
                p1.SetValue("Statement", Root.selectSingleNode("//Root/CreateStatement").text);
                p1.SetValue("Payment", Root.selectSingleNode("//Root/CreatePayment").text);

                if (p1.GetValue("Payment") == "STP Transfer Payment") {
                    var PaymentAccount = sql.Fill("SELECT\r\n" +
                        "	DISTINCT\r\n" +
                        "	UserAccountCode [Account_No],\r\n" +
                        "	ClientName [Payee],\r\n" +
                        "	CheqAccCode [Bank],\r\n" +
                        "	KapptiCode [Region]  \r\n" +
                        "from \r\n" +
                        "	SLAccount \r\n" +
                        "where \r\n" +
                        "	GLAccCode = \'Extern Chq\'\r\n" +
                        "	and CheqAccCode like \'%ABSA%\'\r\n" +
                        "	and isnull(Obsolete, 0 ) = 0\r\n" +
                        "	and CheqAccCode <> \'\'\r\n" +
                        "	and Clasification = \'COMM\'\r\n" +
                        "	and slacccode = " + Root.selectSingleNode("//Root/PaymentAccount").text + "\r\n")

                    if (PaymentAccount != -1) {
                        pBankAcc.ClearGridRows("BankAccount")
                        pBankAcc.AddGridRow("BankAccount", PaymentAccount.Row[0].Account_No + "|" + PaymentAccount.Row[0].Payee + "|" + PaymentAccount.Row[0].Bank + "|" + PaymentAccount.Row[0].Region)
                    }

                }
                else if (p1.GetValue("Payment") == "Link Payment") {
                    var PaymentAccount = sql.Fill("SELECT\r\n" +
                        "	DISTINCT\r\n" +
                        "	UserAccountCode [Account_No],\r\n" +
                        "	ClientName [Payee],\r\n" +
                        "	CheqAccCode [Bank],\r\n" +
                        "	KapptiCode [Region]  \r\n" +
                        "from \r\n" +
                        "	SLAccount \r\n" +
                        "where \r\n" +
                        "	GLAccCode = \'Extern Chq\'\r\n" +
                        "	and CheqAccCode not like \'%ABSA%\'\r\n" +
                        "	and isnull(Obsolete, 0 ) = 0\r\n" +
                        "	and CheqAccCode <> \'\'\r\n" +
                        "	and Clasification = \'COMM\'\r\n" +
                        "	and slacccode = " + Root.selectSingleNode("//Root/PaymentAccount").text + "\r\n")

                    if (PaymentAccount != -1) {
                        pBankAcc.ClearGridRows("BankAccount")
                        pBankAcc.AddGridRow("BankAccount", PaymentAccount.Row[0].Account_No + "|" + PaymentAccount.Row[0].Payee + "|" + PaymentAccount.Row[0].Bank + "|" + PaymentAccount.Row[0].Region)
                    }
                }
                cntx = "GetCalcDetails 3";

                cntx = "Get Stock Details"
                SdtCalc.ClearGridRows("StockDet")
                var Stock = Root.selectNodes("Stock");
                for (sRow = 0; sRow < Stock.length; sRow++) {
                    StockItem = Stock.item(sRow);
                    var StockInvoiceNo = StockItem.getAttribute("StockInvoiceNo");

                    var stockRow = p0.GetValue("dProject") + "|" + StockInvoiceNo + "|"
                        + Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/EndDate").text + "|"
                        + Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/DaysFinanced").text + "|"
                        + Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/InvAmount").text + "|"
                        + Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/Interest").text + "|"
                        + Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/AdminFee").text + "|"
                        + Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/Tonage").text + "|"
                        + Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/ValueIncVat").text + "|"
                        + Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/ValueExclVat").text + "|"
                        + Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/VatAmount").text + "|"
                        + SdtCalc.GetValue("FSANo") + "|" + SdtCalc.GetValue("Measurement");
                    SdtCalc.AddGridRow("StockDet", stockRow);
                }
            }
            if (Root.selectSingleNode("//Root/TransType").text == "COFCO Calculation") {
                p1.SetValue("TransDate", Root.selectSingleNode("//Root/TransDate").text);
                p1.SetValue("ActualRate", Root.selectSingleNode("//Root/ActualInt").text);
                p1.SetValue("AdminFee", Root.selectSingleNode("//Root/AdminFeePTon").text);

                // GlobalVar.RootNode = Root.selectSingleNode("//Root/ContractNo").text;

                if (GlobalVar.FormName == "pCOFCO") {
                    pCOFCO.SetValue("FSANo", Root.selectSingleNode("//Root/ContractNo").text);
                } else if (GlobalVar.FormName == "SdtCalc") {
                    SdtCalc.SetValue("FSANo", Root.selectSingleNode("//Root/ContractNo").text);

                }
                p1.SetValue("Vat", "");
                p1.SetValue("PayTol", Root.selectSingleNode("//Root/PaymentToll").text);
                p1.SetValue("TypeComm", Root.selectSingleNode("//Root/TypeComm").text);
                p1.SetValue("CalcTpe", Root.selectSingleNode("//Root/TransType").text);

                pCOFCO.ClearGridRows("StockDet")
                var Stock = Root.selectNodes("Stock");
                for (sRow = 0; sRow < Stock.length; sRow++) {
                    StockItem = Stock.item(sRow);
                    var StockInvoiceNo = StockItem.getAttribute("StockInvoiceNo");

                    var stockRow = StockInvoiceNo + "|" + Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/EndDate").text + "|"
                        + Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/DaysFinanced").text + "|"
                        + Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/InvAmount").text + "|"
                        + Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/PriceAdjust").text + "|"
                        + Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/AmtExclPA").text + "|"
                        + Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/Interest").text + "|"
                        + Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/AdminFee").text + "|"
                        + Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/Tonage").text + "|"
                        + Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/ValToClient").text + "|"
                        + Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/ChqLocalAmt").text + "|"
                        + SdtCalc.GetValue("FSANo") + "|" + pCOFCO.GetValue("Measurement");
                    pCOFCO.AddGridRow("StockDet", stockRow);
                }

            }

        }
    }
    else if (p0.GetValue("Action") == "FINALIZE" && p0.GetSelectedRows("DiscCalcs") != "" && p0.GetSelectedRows("DiscCalcs").split(";")[0].split("|")[0] == "Finished") {
        p0.NextPage = p4;
        PFECalcRespons(dataNo, p4, p0);
    }
    else if (p0.GetValue("Action") == "FINALIZE" && p0.GetSelectedRows("DiscCalcs") != "" && p0.GetSelectedRows("DiscCalcs").split(";")[0].split("|")[0] != "Finished" && p0.GetSelectedRows("DiscCalcs").split(";")[0].split("|")[0] != "Error: Please check error email") {
        p0.AddInvalidMessage("Still waiting for PFE calc")
    }
    else if (p0.GetValue("Action") == "FINALIZE" && p0.GetSelectedRows("DiscCalcs") != "" && p0.GetSelectedRows("DiscCalcs").split(";")[0].split("|")[0] == "Error: Please check error email") {
        p0.NextPage = p6
        ManualCalc.value = true;
    }

}
function FSAChange() {
    cntx = "FSAChange";
    if (GlobalVar.FormName == "pCOFCO") {
        GlobalVar.FSAvalue = pCOFCO.getValue("FSANo")

    } else {

        GlobalVar.FSAvalue = SdtCalc.getValue("FSANo")

    }

}
function GetDiscountCalcs() {
    p0.ClearGridRows("DiscCalcs");
    var object = sql.GetScalar("Select ObjectNo From [Object] where Code = \'StorageData\'");
    var Calcs = sql.Fill("Select (SELECT TOP 1 Status FROM MessageQueue  WHERE InstanceRefNo = SD.DataNo and ObjectNo = " + object + " ORDER BY CreateDate DESC)[Status],\r\n" +
        "SD.DataNo,SD.InstanceRefNo,PA.ProjectDescription,SD.[Description],SD.Tag,OCR.RequesterUserID From StorageData SD \r\n" +
        "Join ObjectChangeRequest OCR on OCR.ObjectNo = " + object + " and OCR.InstanceRefNo = SD.DataNo \r\n" +
        "JOIN ProjectA PA on PA.ProjectCode = SD.InstanceRefNo \r\n" +
        "Where SD.SchemaNo = 10 and OCR.Status = 0 and SD.[Description] Like \'%Delivered Stock%\' AND SD.InstanceRefNo = " + p0.GetValue("dProject") + "\r\n")

    if (Calcs == -1) {
        p0.AddGridRow("DiscCalcs", "|||No Discount Calcs Outstanding||");
    }
    else {
        for (i = 0; i < Calcs.Row.length; i++) {

            switch (Calcs.Row[i].Status) {
                case "0":
                    var Status = "Pending";
                    break;
                case "1":
                    var Status = "Sent";
                    break;
                case "2":
                    var Status = "Finished";
                    break;
                case "3":
                    var Status = "Error: Please check error email";
                    break;
                default:
                    var Status = "Error: Please check error email";
                    break;
            }


            p0.AddGridRow("DiscCalcs", Status + "|" + trim(Calcs.Row[i].DataNo) + "|" + Calcs.Row[i].InstanceRefNo + "|" + Calcs.Row[i].ProjectDescription + "|" + Calcs.Row[i].Description + "|" + Calcs.Row[i].RequesterUserID);
        }
    }

}
function addStockLines() {
    cntx = "addStockLines"
    SdtCalc.SetEnabled("EditBut", true);

    if (GlobalVar.FormName == "SdtCalc") {
        var type = SdtCalc.getValue("FSANo")
    } else if (GlobalVar.FormName == "pCOFCO") {
        var type = pCOFCO.getValue("FSANo")
    }

    if (type != '') {
        if (p1.GetValue("CalcTpe") == "Standard Delivered Calculation") {
            if (SdtCalc.GetValue("Ton").replace(/\s/g, '') != "" && SdtCalc.GetValue("Ton").replace(/\s/g, '') != "0" && SdtCalc.GetValue("InvoiceNo").replace(/\s/g, '') != "" && SdtCalc.GetValue("InvAmount").replace(/\s/g, '') != "" != "" && SdtCalc.GetValue("InvAmount").replace(/\s/g, '') != "0" != "" && SdtCalc.GetValue("EndDate") != null) {
                var DaysInYear = 365;

                if (SdtCalc.GetValue("InvoiceNo").length < 20) {
                    var getInvoiceBalance = sql.Fill("select \r\n" +
                        "	round(sum(ReportAmount), 2) as \'Balance\',\r\n" +
                        "	SLAccCode\r\n" +
                        "from \r\n" +
                        "	transdet \r\n" +
                        "where \r\n" +
                        "	SLAccCode in (select SLAccCode from SLAccount where GLAccCode = \'849\' and RefCode = \'" + SdtCalc.GetValue("InvoiceNo") + "\' and isnull(Obsolete, 0) = 0)\r\n" +
                        "group by \r\n" +
                        "	SLAccCode\r\n" +
                        "having \r\n" +
                        "	round(sum(ReportAmount), 2) <> 0\r\n")

                    var getContractNo = sql.Fill("SELECT ContractNo FROM [Contract] WHERE Description = \'" + GlobalVar.FSAvalue + "\' AND ProjectCode = \'" + p0.getValue("dProject") + "\'");
                    var getTonnageUsed = sql.Fill("SELECT ISNULL((SUM(StockQuantity)),0) AS [StockQuantity] FROM TransDet_FINANCIALS WHERE SLAccCode IN (SELECT SLAccCode FROM SLAccount WHERE AccTypeNo = 11 AND ContractNo = \'" + getContractNo.Row[0].ContractNo + "\') AND ProjectCode = \'" + p0.getValue("dProject") + "\'");
                    var getFSATonnage = sql.Fill("SELECT ISNULL(AdjustedQuantity,0) AS [AdjustedQuantity] FROM Contract WHERE ContractNo = \'" + getContractNo.Row[0].ContractNo + "\'");
                    var getReservedTonnage = sql.Fill("SELECT ISNULL(SUM(StockQuantity), 0) AS [StockQuantity] \r\n" +
                        "FROM ReservedStock\r\n" +
                        "WHERE ObjectNo = \'52\'\r\n" +
                        "AND InstanceRefNo = \'" + getContractNo.Row[0].ContractNo + "\'\r\n");

                    if (getTonnageUsed == -1) {
                        var UseQty = 0;
                    } else {
                        var UseQty = getTonnageUsed.Row[0].StockQuantity
                    }

                    if (getReservedTonnage == -1) {
                        var ReservedQty = 0;
                    } else {
                        var ReservedQty = getReservedTonnage.Row[0].StockQuantity;
                    }

                    if (getContractNo != -1 || getFSATonnage != -1) {
                        var GridValues = SdtCalc.GetValue("StockDet");
                        GridValues = GridValues.split(";");
                        var LineQty = 0;
                        for (var a = 0; a < GridValues.length; a++) {
                            var GridValuesX = GridValues[a].split("|");
                            //if (GridValues.length != 1) {
                            if (GridValuesX[11] == GlobalVar.FSAvalue) {
                                LineQty = LineQty + new Number(GridValuesX[7]);

                            }
                            //}
                        }
                        var AvailibleTonnage = (new Number(getFSATonnage.Row[0].AdjustedQuantity) - new Number(UseQty) - new Number(SdtCalc.GetValue("Ton")) - new Number(LineQty) - new Number(ReservedQty))

                        if (AvailibleTonnage < 0) {
                            JSMsg("Insufficient stock quantity still availible on the FSA contract: \'" + GlobalVar.FSAvalue + "\' \r\n Please select another FSA Contract or adjust quantity \r\n StockQuantity Still availible: \'" + AvailibleTonnage + "\'")
                        } else {

                            if (getInvoiceBalance != -1) {
                                if (getInvoiceBalance.Row[0].Balance != 0) {
                                    JSMsg("Invoice number " + SdtCalc.GetValue("InvoiceNo") + " already contains a balance! \r\n" +
                                        "SL account " + getInvoiceBalance.Row[0].SLAccCode);
                                }
                            }
                            else {
                                var InterestRate = p1.GetValue("ActualRate") / 100
                                var InvoiceNo = SdtCalc.GetValue("InvoiceNo");
                                var EndDate = sql.SQLDate(new Date(SdtCalc.GetValue("EndDate")), 1);
                                var Days = SdtCalc.GetValue("DaysFinanced");
                                var InvAmount = new Number(SdtCalc.GetValue("InvAmount"));
                                var Interest = InvAmount * InterestRate * (Days / DaysInYear)
                                Interest = Math.round(Interest * 100) / 100;
                                var Ton = new Number(SdtCalc.GetValue("Ton"));
                                var Measurement = SdtCalc.GetValue("Measurement");
                                var AdminFee = vat_obj.COST_AddVat(new Number(p1.GetValue("AdminFee")) * Ton)
                                AdminFee = Math.round(AdminFee * 100) / 100;
                                var ValInclVAT = InvAmount - Interest - AdminFee
                                ValInclVAT = Math.round(ValInclVAT * 100) / 100;
                                var VAT = new Number(p1.GetValue("Vat").replace("%", ""))
                                var AmountEcxlVAT = (ValInclVAT * 100) / (100 + VAT)
                                AmountEcxlVAT = Math.round(AmountEcxlVAT * 100) / 100;
                                var VATAmount = AmountEcxlVAT * (VAT / 100)
                                VATAmount = Math.round(VATAmount * 100) / 100;

                                SdtCalc.AddGridRow("StockDet", p0.GetValue("dProject") + "|" + trim(InvoiceNo) + "|" + EndDate + "|" + Days + "|" + InvAmount + "|" + Interest + "|" + AdminFee + "|" + Ton + "|" + ValInclVAT + "|" + AmountEcxlVAT + "|" + VATAmount + "|" + GlobalVar.FSAvalue + "|" + Measurement);

                                SdtCalc.SetValue("Measurement", 0);
                                SdtCalc.SetValue("Ton", 0);
                                SdtCalc.SetValue("InvoiceNo", "");
                                SdtCalc.SetValue("EndDate", "");
                                SdtCalc.SetValue("DaysFinanced", 0);
                                SdtCalc.SetValue("InvAmount", 0);
                            }
                        }
                    } else {
                        JSMsg("Incorrect details captured on FSA contract: \'" + GlobalVar.FSAvalue)
                    }
                } else {
                    JSMsg("Invoice numbers can only be 20 characters long.\r\nPlease ensure all Invoice numbers are less that 20 characters");
                }
            }
            else {
                JSMsg("Please populate all the required fields*")
            }
        }
        else {

            var DaysInYear = 365;
            var DateString = sql.SQLDate(new Date(pCOFCO.GetValue("EndDate")), 1)
            DateString = DateString.split("-")

            var InvNo = pCOFCO.GetValue("InvoiceNo") + "_" + DateString[0] + DateString[1] + DateString[2]
            if (InvNo.length < 20) {
                var getInvoiceBalance = sql.Fill("select \r\n" +
                    "	round(sum(ReportAmount), 2) as \'Balance\',\r\n" +
                    "	SLAccCode\r\n" +
                    "from \r\n" +
                    "	transdet \r\n" +
                    "where \r\n" +
                    "	SLAccCode in (select SLAccCode from SLAccount where GLAccCode = \'849\' and RefCode = \'" + pCOFCO.GetValue("InvoiceNo") + "\' and isnull(Obsolete, 0) = 0)\r\n" +
                    "group by \r\n" +
                    "	SLAccCode\r\n" +
                    "having \r\n" +
                    "	round(sum(ReportAmount), 2) <> 0\r\n")

                var getContractNo = sql.Fill("SELECT ContractNo FROM [Contract] WHERE Description = \'" + GlobalVar.FSAvalue + "\' AND ProjectCode = \'" + p0.getValue("dProject") + "\'");
                var getTonnageUsed = sql.Fill("SELECT ISNULL((SUM(StockQuantity)),0) AS [StockQuantity] FROM TransDet_FINANCIALS WHERE SLAccCode IN (SELECT SLAccCode FROM SLAccount WHERE AccTypeNo = 11 AND ContractNo = \'" + getContractNo.Row[0].ContractNo + "\') AND ProjectCode = \'" + p0.getValue("dProject") + "\'");
                var getFSATonnage = sql.Fill("SELECT ISNULL(AdjustedQuantity,0) AS [AdjustedQuantity] FROM Contract WHERE ContractNo = \'" + getContractNo.Row[0].ContractNo + "\'");
                var getReservedTonnage = sql.Fill("SELECT ISNULL(SUM(StockQuantity), 0) AS [StockQuantity] \r\n" +
                    "FROM ReservedStock\r\n" +
                    "WHERE ObjectNo = \'52\'\r\n" +
                    "AND InstanceRefNo = \'" + getContractNo.Row[0].ContractNo + "\'\r\n");

                if (getTonnageUsed == -1) {
                    var UseQty = 0;
                } else {
                    var UseQty = getTonnageUsed.Row[0].StockQuantity
                }
                if (getContractNo != -1 || getFSATonnage != -1) {
                    var GridValues = pCOFCO.GetValue("StockDet");
                    GridValues = GridValues.split(";");
                    var LineQty = 0;
                    for (var a = 0; a < GridValues.length; a++) {
                        var GridValuesX = GridValues[a].split("|");
                        if (GridValues.length != 1) {
                            if (GridValuesX[12] == GlobalVar.FSAvalue) {
                                LineQty = LineQty + new Number(GridValuesX[8]);

                            }
                        }
                    }
                    var AvailibleTonnage = (new Number(getFSATonnage.Row[0].AdjustedQuantity) - new Number(UseQty) - new Number(pCOFCO.GetValue("Ton")) - new Number(LineQty))
                    if (AvailibleTonnage < 0) {
                        JSMsg("Insufficient stock quantity still availible on the FSA contract: \'" + GlobalVar.FSAvalue + "\' \r\n Please select another FSA Contract or adjust quantity \r\n StockQuantity Still availible: \'" + AvailibleTonnage + "\'")
                    } else {
                        if (getInvoiceBalance != -1) {
                            if (getInvoiceBalance.Row[0].Balance != 0) {
                                JSMsg("Invoice number " + pCOFCO.GetValue("InvoiceNo") + " already contains a balance! \r\n" +
                                    "SL account " + getInvoiceBalance.Row[0].SLAccCode);
                            }
                        } else {
                            var InvoiceNo = InvNo
                            var EndDate = sql.SQLDate(new Date(pCOFCO.GetValue("EndDate")), 1);
                            var Days = pCOFCO.GetValue("DaysFinanced");
                            var InvAmount = new Number(pCOFCO.GetValue("InvAmount"));
                            InvAmount = Math.round(InvAmount * 100) / 100;
                            var PriceAdjustment = new Number(pCOFCO.GetValue("PriceAdjust"));
                            PriceAdjustment = Math.round(PriceAdjustment * 100) / 100;
                            var AmtInclPA = InvAmount + PriceAdjustment
                            AmtInclPA = Math.round(AmtInclPA * 100) / 100;
                            var Interest = AmtInclPA * (new Number(p1.GetValue("ActualRate")) / 100) * Days / DaysInYear
                            Interest = Math.round(Interest * 100) / 100;
                            var Ton = new Number(pCOFCO.GetValue("Ton"));
                            var Measurement = pCOFCO.GetValue("Measurement");
                            var AdminFee = vat_obj.COST_AddVat(new Number(p1.GetValue("AdminFee")) * Ton)
                            AdminFee = Math.round(AdminFee * 100) / 100;
                            var ChqLocal = AmtInclPA - Interest - AdminFee
                            ChqLocal = Math.round(ChqLocal * 100) / 100;
                            var ValDueToClient = ChqLocal - PriceAdjustment
                            ValDueToClient = Math.round(ValDueToClient * 100) / 100;

                            pCOFCO.AddGridRow("StockDet", trim(InvoiceNo) + "|" + EndDate + "|" + Days + "|" + InvAmount + "|" + PriceAdjustment + "|" + AmtInclPA + "|" + Interest + "|" + AdminFee + "|" + Ton + "|" + ValDueToClient + "|" + ChqLocal + "|" + GlobalVar.FSAvalue + "|" + Measurement);

                            pCOFCO.SetValue("Ton", 0);
                            pCOFCO.SetValue("InvoiceNo", "");
                            pCOFCO.SetValue("InvAmount", 0);
                            pCOFCO.SetValue("PriceAdjust", 0);
                        }
                    }
                } else {
                    JSMsg("Incorrect details captured on FSA contract: \'" + GlobalVar.FSAvalue)
                }
            } else {
                JSMsg("Invoice numbers can only be 20 characters long.\r\nPlease ensure all Invoice numbers are less that 20 characters");
            }

        }
    } else {
        JSMsg("Please select the relevant FSA Number.")
    }

}
function AmendEdit() {

    if (p1.GetValue("CalcTpe") == "Standard Delivered Calculation") {
        if (SdtCalc.GetValue("StockDet") != "" && p0.GetValue("Action") == "AMEND EXISTING") {
            var RowCalc = SdtCalc.GetValue("StockDet").split(";");
            SdtCalc.ClearGridRows("StockDet");

            for (a = 0; a < RowCalc.length; a++) {
                var FromDate = new Date(p1.GetValue("TransDate"))
                FromDate = sql.SQLDate(FromDate, 1)
                var ToDate = RowCalc[a].split("|")[2]
                var dayDiff = sql.GetScalar("Select DATEDIFF(dd,\'" + FromDate + "\',\'" + ToDate + "\') [Days]\r\n")
                var InterestRate = p1.GetValue("ActualRate") / 100
                var InvoiceNo = RowCalc[a].split("|")[1]
                var EndDate = ToDate;
                var Days = dayDiff;
                var InvAmount = new Number(RowCalc[a].split("|")[4]);
                var Interest = InvAmount * InterestRate * (Days / 365)
                Interest = Math.round(Interest * 100) / 100;
                var Ton = new Number(RowCalc[a].split("|")[7]);
                try {
                    //Measurement on Row  12, try is for the records that dont have unit of measurement added.
                    var Measure = RowCalc[a].split("|")[12];
                } catch (ex) { var Measure = "" }
                var AdminFee = vat_obj.COST_AddVat(new Number(p1.GetValue("AdminFee")) * Ton)
                AdminFee = Math.round(AdminFee * 100) / 100;
                var ValInclVAT = InvAmount - Interest - AdminFee
                ValInclVAT = Math.round(ValInclVAT * 100) / 100;
                var VAT = new Number(p1.GetValue("Vat").replace("%", ""))
                var AmountEcxlVAT = (ValInclVAT * 100) / (100 + VAT)
                AmountEcxlVAT = Math.round(AmountEcxlVAT * 100) / 100;
                var VATAmount = AmountEcxlVAT * (VAT / 100)
                VATAmount = Math.round(VATAmount * 100) / 100;

                SdtCalc.AddGridRow("StockDet", p0.GetValue("dProject") + "|" + trim(InvoiceNo) + "|" + EndDate + "|" + Days + "|" + InvAmount + "|" + Interest + "|" + AdminFee + "|" + Ton + "|" + ValInclVAT + "|" + AmountEcxlVAT + "|" + VATAmount + "|" + GlobalVar.FSAvalue + "|" + Measure);
            }
        }
    }

}//NEW
function daysEvent() {
    var PaymentToll = new Number(p1.GetValue("PayTol"));
    var ToDate = new Date(p1.GetValue("TransDate"))

    if (p1.GetValue("CalcTpe") == "COFCO Calculation") {
        var FromDate = new Date(pCOFCO.GetValue("EndDate"))
        var one_day = 1000 * 60 * 60 * 24
        var dayDiff = (Math.ceil((FromDate.getTime() - ToDate.getTime()) / (one_day))) + PaymentToll
        if (!isNaN(dayDiff))
            pCOFCO.SetValue("DaysFinanced", dayDiff)
    }
    else {
        var FromDate = new Date(SdtCalc.GetValue("EndDate"))
        var one_day = 1000 * 60 * 60 * 24
        var dayDiff = (Math.ceil((FromDate.getTime() - ToDate.getTime()) / (one_day))) + PaymentToll
        if (!isNaN(dayDiff))
            SdtCalc.SetValue("DaysFinanced", dayDiff)
    }
}
function ImportStock() {
    if (p1.GetValue("CalcTpe") == "Standard Delivered Calculation") {
        try {
            SdtCalc.ClearGridRows("StockDet")

            var filePath = SdtCalc.GetValue("stockFile");
            var excel = new ActiveXObject("Excel.Application");
            var workbook = excel.Workbooks.Open(filePath);
            var sheet = workbook.Sheets(1);
            var checkError = false;
            offsetLine = 4;
            var InvoiceLength = false;

            var DaysInYear = 365;

            for (var i = offsetLine; typeof (sheet.Cells(i, 1).Value) != "undefined"; i++) {

                var PaymentToll = new Number(p1.GetValue("PayTol"));
                var ToDate = new Date(p1.GetValue("TransDate"))
                var EndDate = new Date(sheet.Cells(i, 2).Value)
                var one_day = 1000 * 60 * 60 * 24
                var dayDiff = (Math.ceil((EndDate.getTime() - ToDate.getTime()) / (one_day))) + PaymentToll

                if (!isNaN(dayDiff))
                    var Days = dayDiff

                if (sheet.Cells(i, 1).Value.length > 20) {
                    InvoiceLength = true;
                }

                EndDate = sql.SQLDate(new Date(sheet.Cells(i, 2).Value), 1)

                var InterestRate = p1.GetValue("ActualRate") / 100

                var InvoiceNo = sheet.Cells(i, 1).Value

                var InvAmount = new Number(sheet.Cells(i, 4).Value)
                InvAmount = Math.round(InvAmount * 100) / 100;

                var Interest = InvAmount * InterestRate * (Days / DaysInYear)
                Interest = Math.round(Interest * 100) / 100;

                var Ton = new Number(sheet.Cells(i, 3).Value);

                var Measurement = sheet.Cells(i, 5).Value;

                var AdminFee = vat_obj.COST_AddVat(new Number(p1.GetValue("AdminFee")) * Ton)
                AdminFee = Math.round(AdminFee * 100) / 100;

                var ValInclVAT = InvAmount - Interest - AdminFee
                ValInclVAT = Math.round(ValInclVAT * 100) / 100;

                var VAT = new Number(p1.GetValue("Vat").replace("%", ""))

                var AmountEcxlVAT = (ValInclVAT * 100) / (100 + VAT)
                AmountEcxlVAT = Math.round(AmountEcxlVAT * 100) / 100;

                var VATAmount = AmountEcxlVAT * (VAT / 100)
                VATAmount = Math.round(VATAmount * 100) / 100;

                var getInvoiceBalance = sql.Fill("select \r\n" +
                    "	round(sum(ReportAmount), 2) as \'Balance\',\r\n" +
                    "	SLAccCode\r\n" +
                    "from \r\n" +
                    "	transdet \r\n" +
                    "where \r\n" +
                    "	SLAccCode in (select SLAccCode from SLAccount where GLAccCode = \'849\' and RefCode = \'" + InvoiceNo + "\' and isnull(Obsolete, 0) = 0)\r\n" +
                    "group by \r\n" +
                    "	SLAccCode\r\n" +
                    "having \r\n" +
                    "	round(sum(ReportAmount), 2) <> 0\r\n")

                if (getInvoiceBalance != -1) {
                    if (getInvoiceBalance.Row[0].Balance != 0) {
                        JSMsg("Invoice number " + SdtCalc.GetValue("InvoiceNo") + " already contains a balance! \r\n" +
                            "SL account " + getInvoiceBalance.Row[0].SLAccCode);
                        break;
                    }
                }

                var getContractNo = sql.Fill("SELECT ContractNo FROM [Contract] WHERE Description = \'" + GlobalVar.FSAvalue + "\'");
                var getTonnageUsed = sql.Fill("SELECT ISNULL((SUM(StockQuantity)),0) AS [StockQuantity] FROM TransDet_FINANCIALS WHERE SLAccCode IN (SELECT SLAccCode FROM SLAccount WHERE AccTypeNo = 11 AND ContractNo = \'" + getContractNo.Row[0].ContractNo + "\') AND ProjectCode = \'" + p0.getValue("dProject") + "\'");
                var getFSATonnage = sql.Fill("SELECT ISNULL(AdjustedQuantity,0) AS [AdjustedQuantity] FROM Contract WHERE ContractNo = \'" + getContractNo.Row[0].ContractNo + "\'");
                if (getTonnageUsed == -1) {
                    var UseQty = 0;
                } else {
                    var UseQty = getTonnageUsed.Row[0].StockQuantity
                }
                if (getContractNo != -1 || getFSATonnage != -1) {
                    var GridValues = SdtCalc.GetValue("StockDet");
                    GridValues = GridValues.split(";");
                    var LineQty = 0;
                    for (var a = 0; a < GridValues.length; a++) {
                        var GridValuesX = GridValues[a].split("|");
                        if (GridValues.length != 1) {
                            LineQty = LineQty + new Number(GridValuesX[8]);
                        }
                    }
                    var AvailibleTonnage = (new Number(getFSATonnage.Row[0].AdjustedQuantity) - new Number(UseQty) - new Number(Ton) - new Number(LineQty))
                    if (AvailibleTonnage < 0) {
                        JSMsg("Insufficient stock quantity still availible on the FSA contract: \'" + GlobalVar.FSAvalue + "\' \r\n Please select another FSA Contract or adjust quantity \r\n StockQuantity Still availible: \'" + AvailibleTonnage + "\'")
                        break;
                    } else {

                        if (InvoiceLength = "False") {
                            //AW to Add Meaasurement.
                            SdtCalc.AddGridRow("StockDet", p0.GetValue("dProject") + "|" + trim(InvoiceNo) + "|" + EndDate + "|" + Days + "|" + InvAmount + "|" + Interest + "|" + AdminFee + "|" + Ton + "|" + ValInclVAT + "|" + AmountEcxlVAT + "|" + VATAmount + "|" + GlobalVar.FSAvalue + "|" + Measurement);
                        } else {
                            JSMsg("Invoice numbers can only be 20 characters long.\r\nPlease ensure all Invoice numbers are less that 20 characters")
                            break;
                        }
                    }
                } else {
                    JSMsg("Incorrect details captured on FSA contract: \'" + GlobalVar.FSAvalue)
                    break;
                }
            }

            try { workbook.Close(true); } catch (re) { }
            try { excel.Quit(); } catch (re) { }
        }
        catch (ex) {
            JSMsg(ex.message);
        }
        SdtCalc.SetValue("stockFile", "")
    }
}
function BuildChangeXML() {
    cntx = "BuildChangeXML"
    var xmlString = "<Change><DeliveredStockCalc> <ProjectCode Value=\"\"" + p0.GetValue("dProject") + "\"\"/>  <Contract No=\"\"" + trim(GlobalVar.FSAvalue) + "\"\"/>  </DeliveredStockCalc><Change>";
    return xmlString;
}
function BuildXML() {

    if (p1.GetValue("CalcTpe") == "Standard Delivered Calculation") {
        var TotInvAmount = 0
        var xmlStringStock = "";
        var getTotals = SdtCalc.GetValue("StockDet").split(";");
        for (a = 0; a < getTotals.length; a++) {
            xmlStringStock = xmlStringStock + "<Stock StockInvoiceNo =\"\"" + getTotals[a].split("|")[1] + "\"\">\r\n"
                + "<Project>" + getTotals[a].split("|")[0] + "</Project>\r\n"
                + "<EndDate>" + getTotals[a].split("|")[2] + "</EndDate>\r\n"
                + "<DaysFinanced>" + getTotals[a].split("|")[3] + "</DaysFinanced>\r\n"
                + "<Tonage>" + getTotals[a].split("|")[7] + "</Tonage>\r\n"
                + "<InvAmount>" + getTotals[a].split("|")[4] + "</InvAmount>\r\n"
                + "<Interest>" + getTotals[a].split("|")[5] + "</Interest>\r\n"
                + "<AdminFee>" + getTotals[a].split("|")[6] + "</AdminFee>\r\n"
                + "<ValueIncVat>" + getTotals[a].split("|")[8] + "</ValueIncVat>\r\n"
                + "<ValueExclVat>" + getTotals[a].split("|")[9] + "</ValueExclVat>\r\n"
                + "<VatAmount>" + getTotals[a].split("|")[10] + "</VatAmount>\r\n"
                + "<FSANumber>" + getTotals[a].split("|")[11] + "</FSANumber>\r\n"
                + "<Measurement>" + getTotals[a].split("|")[12] + "</Measurement>\r\n"
                + "</Stock>";

            TotInvAmount += new Number(getTotals[a].split("|")[4]);
            var TotInvAmount = Math.round(TotInvAmount * 100) / 100;
        }

        var PaymentAccResult = pBankAcc.GetSelectedRows("BankAccount");
        var PayAcckArr = PaymentAccResult.split(";");

        cntx = "Get Selected Payment Account No"
        for (var s = 0; s < PayAcckArr.length; s++) {
            var PayAcc = PayAcckArr[s].split("|");
            var Account = PayAcc[0]
        }

        cntx = "BuildXML"
        var xmlString = "<Root>"
            + "<ClientName>" + p1.GetValue("ClientName") + "</ClientName>\r\n"
            + "<TransType>" + "Standard Delivered Calculation" + "</TransType>\r\n"
            + "<TransDate>" + sql.SQLDate(new Date(p1.GetValue("TransDate")), 1) + "</TransDate>\r\n"
            + "<PaymentToll>" + p1.getValue("PayTol") + "</PaymentToll>\r\n"
            + "<TypeComm>" + p1.getValue("TypeComm") + "</TypeComm>\r\n"
            + "<DaysFinanced>" + "-" + "</DaysFinanced>\r\n"
            + "<CreateStatement>" + p1.GetValue("Statement") + "</CreateStatement>\r\n"
            + "<CreatePayment>" + p1.GetValue("Payment") + "</CreatePayment>\r\n"
            + "<PaymentAccount>" + Account + "</PaymentAccount>\r\n"
            + "<ActualInt>" + p1.GetValue("ActualRate") + "</ActualInt>\r\n"
            + "<DiscountPrem>" + "-" + "</DiscountPrem>\r\n"
            + "<DiscountRate>" + "-" + "</DiscountRate>\r\n"
            + "<AdminFeePTon>" + p1.GetValue("AdminFee") + "</AdminFeePTon>\r\n"            
            + "<TotalTon>" + p3.GetValue("TotalTon") + "</TotalTon>\r\n"
            + "<VatPercentage>" + p1.GetValue("Vat") + "</VatPercentage>\r\n"
            + "<AmountExclVat>" + p3.GetValue("TotalExclVat") + "</AmountExclVat>\r\n"
            + "<VatTotal>" + p3.GetValue("VatTotal") + "</VatTotal>\r\n"
            + "<TotalInvAmount>" + TotInvAmount + "</TotalInvAmount>\r\n"
            + "<TotalInclVat>" + p3.GetValue("TotalIncVat") + "</TotalInclVat>\r\n"
            + "<ReservedID>" + ReserveDebtors(p0.GetValue("dProject"), TotInvAmount) + "</ReservedID>\r\n"

        xmlString = xmlString + xmlStringStock + "</Root>";
    }
    else {
        var TotInvAmount = 0
        var getTotals = pCOFCO.GetValue("StockDet").split(";");
        var xmlStringStock = "";

        for (a = 0; a < getTotals.length; a++) {
            xmlStringStock = xmlStringStock + "<Stock StockInvoiceNo =\"\"" + getTotals[a].split("|")[0] + "\"\">\r\n"
                + "<Project>" + p0.GetValue("dProject") + "</Project>\r\n"
                + "<EndDate>" + getTotals[a].split("|")[1] + "</EndDate>\r\n"
                + "<DaysFinanced>" + getTotals[a].split("|")[2] + "</DaysFinanced>\r\n"
                + "<InvAmount>" + getTotals[a].split("|")[3] + "</InvAmount>\r\n"
                + "<PriceAdjust>" + getTotals[a].split("|")[4] + "</PriceAdjust>\r\n"
                + "<AmtExclPA>" + getTotals[a].split("|")[5] + "</AmtExclPA>\r\n"
                + "<Interest>" + getTotals[a].split("|")[6] + "</Interest>\r\n"
                + "<AdminFee>" + getTotals[a].split("|")[7] + "</AdminFee>\r\n"
                + "<Tonage>" + getTotals[a].split("|")[8] + "</Tonage>\r\n"
                + "<ValToClient>" + getTotals[a].split("|")[9] + "</ValToClient>\r\n"
                + "<ChqLocalAmt>" + getTotals[a].split("|")[10] + "</ChqLocalAmt>\r\n"
                + "<FSANumber>" + getTotals[a].split("|")[11] + "</FSANumber>\r\n"
                + "<Measurement>" + getTotals[a].split("|")[12] + "</Measurement>\r\n"
                + "</Stock>";

            TotInvAmount += new Number(getTotals[a].split("|")[4]);
            var TotInvAmount = Math.round(TotInvAmount * 100) / 100;

        }

        cntx = "BuildXML"
        var xmlString = "<Root>"
            + "<ClientName>" + p1.GetValue("ClientName") + "</ClientName>\r\n"
            + "<TransType>" + "COFCO Calculation" + "</TransType>\r\n"
            + "<TransDate>" + sql.SQLDate(new Date(p1.GetValue("TransDate")), 1) + "</TransDate>\r\n"
            + "<PaymentToll>" + p1.getValue("PayTol") + "</PaymentToll>\r\n"
            + "<TypeComm>" + p1.getValue("TypeComm") + "</TypeComm>\r\n"
            + "<ActualInt>" + p1.GetValue("ActualRate") + "</ActualInt>\r\n"
            + "<TotalTon>" + p3.GetValue("TotalTon") + "</TotalTon>\r\n"
            + "<AdminFeePTon>" + p3.GetValue("AdminFees") + "</AdminFeePTon>\r\n"
            + "<AmountExclPriceAdjust>" + p3.GetValue("TotalExclVat") + "</AmountExclPriceAdjust>\r\n"
            + "<PriceAdjustTotal>" + p3.GetValue("VatTotal") + "</PriceAdjustTotal>\r\n"
            + "<TotalInclPriceAdjust>" + p3.GetValue("TotalIncVat") + "</TotalInclPriceAdjust>\r\n"
            + "<ValDueToClient>" + p3.GetValue("ValTClient") + "</ValDueToClient>\r\n"
            + "<ChqLocal>" + p3.GetValue("ChqLocal") + "</ChqLocal>\r\n"
            + "<ReservedID>" + ReserveDebtors(GlobalVar.FSAvalue, TotInvAmount) + "</ReservedID>"

        if (typeof (pBankAcc.GetValue("Reference")) != "undefined") {
            xmlString = xmlString + "\r\n<ReferenceAccount>" + pBankAcc.GetValue("Reference") + "</ReferenceAccount>";
        }

        xmlString = xmlString + xmlStringStock + "</Root>";
    }
    return xmlString
}
function GetNextPage() {
    if (p1.getValue("TransDate") == null) {
        p1.AddInvalidMessage("Please choose the correct date.")
    }
    if (GlobalVar.FSAvalue == "") {
        p1.AddInvalidMessage("Please FSA Number.")
    }
    if (p1.getValue("TypeComm") == "") {
        p1.AddInvalidMessage("Please select a commodity")
    }
    if (Popup("System Message", "Do you agree with a Payment Tollerance Days value of " + p1.GetValue("PayTol") + " days?") == 0) {
        p1.AddInvalidMessage("Please go add correct Payment Tollerance Days.")
    }
}
function CommVat() {
    var getComm = sql.Fill("select isnull(VATType, 0) as \'VATType\', StorageCost, Margins, Limits from TypeCom where isnull(InactiveFlag, 0) <> 1 and [description] = \'" + p1.GetValue("TypeComm") + "\'")
    if (getComm == -1) {
        if (p1.getValue("TransDate") == "") {
            JSMsg("Please select Commodity first!")
        }

    }
    else {
        var transDate = dUtil.wiz_js(p1.GetValue("TransDate"));
        if (getComm.Row[0].VATType == "0%") {
            p1.SetValue("Vat", "0%");
        }
        else {
            if (vat_obj.IsRateEffective(new Date(transDate))) {
                p1.SetValue("Vat", vat_obj.UI_DisplayRate());
            }
            else {
                p1.SetValue("Vat", new Number(Math.round(vat_obj.prevrate * 100), 1) + "%");
            }
        }
    }
}
function GetEditDetail() {
    if (p1.GetValue("CalcTpe") == "Standard Delivered Calculation") {
        if (SdtCalc.GetSelectedRows("StockDet") == "") {
            JSMsg("Please select a Line to Edit")
        }
        else {
            var stkItemArray = SdtCalc.GetSelectedRows("StockDet").split("|");
            cntx = "Insert The edit details"

            var PaymentToll = new Number(p1.GetValue("PayTol"));
            var ToDate = new Date(p1.GetValue("TransDate"))
            var FromDate = new Date(SdtCalc.GetValue("EndDate"))
            var one_day = 1000 * 60 * 60 * 24
            var dayDiff = (Math.ceil((FromDate.getTime() - ToDate.getTime()) / (one_day))) + PaymentToll
            if (!isNaN(dayDiff))
                SdtCalc.SetValue("DaysFinanced", dayDiff)
            SdtCalc.SetValue("InvoiceNo", stkItemArray[1]);
            SdtCalc.SetValue("EndDate", stkItemArray[2]);
            SdtCalc.SetValue("InvAmount", stkItemArray[4]);
            SdtCalc.SetValue("Ton", stkItemArray[7]);
            SdtCalc.SetValue("Measurement", stkItemArray[12]);

            var editStock = new ActiveXObject("Scripting.Dictionary");
            var stkArray = SdtCalc.GetValue("StockDet").split(";");
            for (var i = 0; i < stkArray.length; i++) {
                editStock.Add(stkArray[i].split("|")[1], stkArray[i])
            }
            var remline = stkItemArray[1];

            editStock.Remove(remline);
            SdtCalc.ClearGridRows("StockDet");

            var mkeys = (new VBArray(editStock.Keys())).toArray();

            for (var k in mkeys) {
                SdtCalc.AddGridRow("StockDet", editStock.Item(mkeys[k]));
            }

            SdtCalc.SetEnabled("EditBut", false);
        }
    }
    else {
        if (pCOFCO.GetSelectedRows("StockDet") == "") {
            JSMsg("Please select a Line to Edit")
        }
        else {
            var stkItemArray = pCOFCO.GetSelectedRows("StockDet").split("|");

            pCOFCO.SetValue("InvoiceNo", stkItemArray[0]);
            pCOFCO.SetValue("EndDate", stkItemArray[1]);
            pCOFCO.SetValue("DaysFinanced", stkItemArray[2]);
            pCOFCO.SetValue("InvAmount", stkItemArray[3]);
            pCOFCO.SetValue("Ton", stkItemArray[8]);
            pCOFCO.SetValue("Measurement", stkItemArray[12]);
            pCOFCO.SetValue("PriceAdjust", stkItemArray[4]);

            var editStock = new ActiveXObject("Scripting.Dictionary");

            var stkArray = pCOFCO.GetValue("StockDet").split(";");

            for (var i = 0; i < stkArray.length; i++) {
                editStock.Add(stkArray[i].split("|")[1], stkArray[i])
            }

            var remline = stkItemArray[1];

            editStock.Remove(remline);
            pCOFCO.ClearGridRows("StockDet");

            var mkeys = (new VBArray(editStock.Keys())).toArray();

            for (var k in mkeys) {
                pCOFCO.AddGridRow("StockDet", editStock.Item(mkeys[k]));
            }
        }
    }
}
function DeleteStockLines() {
    if (Popup("System Message", "Are you sure you want to delete all the grid rows? ") == 1) {
        SdtCalc.ClearGridRows("StockDet");
        pCOFCO.ClearGridRows("StockDet");
        if (p0.GetValue("Action") == "AMEND EXISTING") {
            var discountCalcArray = p0.GetSelectedRows("DiscCalcs").split(";");
            var dataNo = discountCalcArray[0].split("|")[1];
            DeleteReserveDebtors("DataNo", dataNo);
        }

    }

}
function GetPaymentAccount() {
    if (p1.getValue("TransDate") == null) {
        p1.AddInvalidMessage("Please choose the correct date.")
    }
    if (p1.getValue("TypeComm") == "") {
        p1.AddInvalidMessage("Please select a commodity")
    }
    if (Popup("System Message", "Do you agree with a Payment Tollerance Days value of " + p1.GetValue("PayTol") + " days?") == 0) {
        p1.AddInvalidMessage("Please go add correct Payment Tollerance Days.")
    }

    if (p0.GetValue("Action") != "AMEND EXISTING") {
        pBankAcc.ClearGridRows("BankAccount")
        if (p1.GetValue("Payment") == "STP Transfer Payment") {
            var Accounts = sql.Fill("SELECT\r\n" +
                "	DISTINCT\r\n" +
                "	UserAccountCode [Account_No],\r\n" +
                "	ClientName [Payee],\r\n" +
                "	CheqAccCode [Bank],\r\n" +
                "	KapptiCode [Region]  \r\n" +
                "from \r\n" +
                "	SLAccount \r\n" +
                "where \r\n" +
                "	GLAccCode = \'Extern Chq\'\r\n" +
                "	and CheqAccCode like \'%ABSA%\'\r\n" +
                "	and isnull(Obsolete, 0 ) = 0\r\n" +
                "	and CheqAccCode <> \'\'\r\n" +
                "	and Clasification = \'COMM\'\r\n" +
                "   and ClientName like \'%" + pBankAcc.GetValue("BeneficiaryFilter") + "%\'" +
                "	Order By Payee\r\n")


            if (Accounts != -1) {
                for (var zz = 0; zz < Accounts.Row.length; zz++) {
                    pBankAcc.AddGridRow("BankAccount", Accounts.Row[zz].Account_No + "|" + Accounts.Row[zz].Payee + "|" + Accounts.Row[zz].Bank + "|" + Accounts.Row[zz].Region)
                }
            }
        }
        if (p1.GetValue("Payment") == "Link Payment") {
            var Accounts = sql.Fill("SELECT\r\n" +
                "	DISTINCT\r\n" +
                "	UserAccountCode [Account_No],\r\n" +
                "	ClientName [Payee],\r\n" +
                "	CheqAccCode [Bank],\r\n" +
                "	KapptiCode [Region]  \r\n" +
                "from \r\n" +
                "	SLAccount \r\n" +
                "where \r\n" +
                "	GLAccCode = \'Extern Chq\'\r\n" +
                "	and CheqAccCode not like \'%ABSA%\'\r\n" +
                "	and isnull(Obsolete, 0 ) = 0\r\n" +
                "	and CheqAccCode <> \'\'\r\n" +
                "	and Clasification = \'COMM\'\r\n" +
                "   and ClientName like \'%" + pBankAcc.GetValue("BeneficiaryFilter") + "%\'" +
                "	Order By Payee\r\n")


            if (Accounts != -1) {
                for (var zz = 0; zz < Accounts.Row.length; zz++) {
                    pBankAcc.AddGridRow("BankAccount", Accounts.Row[zz].Account_No + "|" + Accounts.Row[zz].Payee + "|" + Accounts.Row[zz].Bank + "|" + Accounts.Row[zz].Region)
                }
            }
        }
        if (p1.GetValue("Payment") == "No Payment") {
            p1.NextPage = SdtCalc;

        }
    }
    else {
        if (pBankAcc.GetValue("BankAccount") != "") {
            var Account = pBankAcc.GetValue("BankAccount").split(";");
            var AccountArr = Account[0].split("|")[2];
            var AccountColArr = AccountArr.split(" ")
            var BankName = AccountColArr[0]
            if (p1.GetValue("Payment") == "STP Transfer Payment" && BankName.toUpperCase() != "ABSA") {
                p1.NextPage = pBankAcc;
                pBankAcc.ClearGridRows("BankAccount")
                var Accounts = sql.Fill("SELECT\r\n" +
                    "	DISTINCT\r\n" +
                    "	UserAccountCode [Account_No],\r\n" +
                    "	ClientName [Payee],\r\n" +
                    "	CheqAccCode [Bank],\r\n" +
                    "	KapptiCode [Region]  \r\n" +
                    "from \r\n" +
                    "	SLAccount \r\n" +
                    "where \r\n" +
                    "	GLAccCode = \'Extern Chq\'\r\n" +
                    "	and CheqAccCode like \'%ABSA%\'\r\n" +
                    "	and isnull(Obsolete, 0 ) = 0\r\n" +
                    "	and CheqAccCode <> \'\'\r\n" +
                    "   and ClientName like \'%" + pBankAcc.GetValue("BeneficiaryFilter") + "%\'" +
                    "	and Clasification = \'COMM\'\r\n")


                if (Accounts != -1) {
                    for (var zz = 0; zz < Accounts.Row.length; zz++) {
                        pBankAcc.AddGridRow("BankAccount", Accounts.Row[zz].Account_No + "|" + Accounts.Row[zz].Payee + "|" + Accounts.Row[zz].Bank + "|" + Accounts.Row[zz].Region)
                    }
                }
            }
            else if (p1.GetValue("Payment") == "Link Payment" && BankName.toUpperCase() == "ABSA") {
                p1.NextPage = pBankAcc;
                pBankAcc.ClearGridRows("BankAccount")
                var Accounts = sql.Fill("SELECT\r\n" +
                    "	DISTINCT\r\n" +
                    "	UserAccountCode [Account_No],\r\n" +
                    "	ClientName [Payee],\r\n" +
                    "	CheqAccCode [Bank],\r\n" +
                    "	KapptiCode [Region]  \r\n" +
                    "from \r\n" +
                    "	SLAccount \r\n" +
                    "where \r\n" +
                    "	GLAccCode = \'Extern Chq\'\r\n" +
                    "	and CheqAccCode not like \'%ABSA%\'\r\n" +
                    "	and isnull(Obsolete, 0 ) = 0\r\n" +
                    "	and CheqAccCode <> \'\'\r\n" +
                    "   and ClientName like \'%" + pBankAcc.GetValue("BeneficiaryFilter") + "%\'" +
                    "	and Clasification = \'COMM\'\r\n")


                if (Accounts != -1) {
                    for (var zz = 0; zz < Accounts.Row.length; zz++) {
                        pBankAcc.AddGridRow("BankAccount", Accounts.Row[zz].Account_No + "|" + Accounts.Row[zz].Payee + "|" + Accounts.Row[zz].Bank + "|" + Accounts.Row[zz].Region)
                    }
                }
            }
        }
        else {
            if (p1.GetValue("Payment") == "No Payment") {
                p1.NextPage = SdtCalc;
            }
        }
    }
}
function TypeCommSet() {
    GlobalVar.TypeCommVal = p1.GetValue("TypeComm")
    FSANumber();
}
function FSANumber() {

    cntx = "Get FSA Contracts Linked to commodity type";
    var getCommodityType = sql.Fill("SELECT CommodityTypeNo FROM TypeCom WHERE Description = \'" + GlobalVar.TypeCommVal + "\' AND InactiveFlag != 1");
    if (getCommodityType != -1 && getCommodityType != '' && getCommodityType != null) {
        var getFSAContract = sql.Fill("SELECT * \r\n" +
            "FROM Contract \r\n" +
            "WHERE ContractTypeNo = 19 \r\n" +
            "AND [Status] = 1 \r\n" +
            "AND ProjectCode = \'" + p0.GetValue("dProject") + "\'\r\n" +
            "AND CommodityTypeNo IN (" + getCommodityType.Row[0].CommodityTypeNo + ") \r\n" +
            "AND (Convert(varchar(10), GETDATE(), 120) <= DT_ExpiryDate OR Convert(varchar(10), GETDATE(), 120) <= DT_ExtensionDate)\r\n")

        if (getFSAContract != -1 && getFSAContract != '') {
            GlobalVar.FSANumbers = ""
            for (var c = 0; c < getFSAContract.Row.length; c++) {
                if (c + 1 != getFSAContract.Row.length)
                    GlobalVar.FSANumbers += getFSAContract.Row[c].Description + "|"
                else
                    GlobalVar.FSANumbers += getFSAContract.Row[c].Description
            }

            if (GlobalVar.FormName == "pCOFCO") {

                pCOFCO.SetItems("FSANo", GlobalVar.FSANumbers.toUpperCase());

            } else if (GlobalVar.FormName == "SdtCalc") {

                SdtCalc.SetItems("FSANo", GlobalVar.FSANumbers.toUpperCase());

            }

        } else {
            p1.AddInvalidMessage("No FSA Contracts For Specified Commodity: " + GlobalVar.TypeCommVal)
        }
    } else {
        p1.AddInvalidMessage("No CommodityTypeNo linked to: " + GlobalVar.TypeCommVal)
    }
    var getComm = sql.Fill("select isnull(VATType, 0) as \'VATType\', StorageCost, Margins, Limits from TypeCom where isnull(InactiveFlag, 0) <> 1 and [description] = \'" + GlobalVar.TypeCommVal + "\'")

    if (getComm == -1) {
        if (p1.getValue("TransDate") == "") {
            JSMsg("Please select Commodity first!")
        }

    }
    else {
        var transDate = dUtil.wiz_js(p1.GetValue("TransDate"));
        if (getComm.Row[0].VATType == "0%") {
            p1.SetValue("Vat", "0%");
            cntx = "no 5";

        }
        else {
            if (vat_obj.IsRateEffective(new Date(transDate))) {
                p1.SetValue("Vat", vat_obj.UI_DisplayRate());
            }
            else {
                p1.SetValue("Vat", new Number(Math.round(vat_obj.prevrate * 100), 1) + "%");
            }
        }
    }
}
function ReloadAccounts() {
    cntx = "Populate Payment account Grid"
    pBankAcc.ClearGridRows("BankAccount")
    if (p1.GetValue("Payment") == "STP Transfer Payment") {
        var Accounts = sql.Fill("SELECT\r\n" +
            "	DISTINCT\r\n" +
            "	UserAccountCode [Account_No],\r\n" +
            "	ClientName [Payee],\r\n" +
            "	CheqAccCode [Bank],\r\n" +
            "	KapptiCode [Region]  \r\n" +
            "from \r\n" +
            "	SLAccount \r\n" +
            "where \r\n" +
            "	GLAccCode = \'Extern Chq\'\r\n" +
            "	and CheqAccCode like \'%ABSA%\'\r\n" +
            "	and isnull(Obsolete, 0 ) = 0\r\n" +
            "	and CheqAccCode <> \'\'\r\n" +
            "	and Clasification = \'COMM\'\r\n" +
            "order by\r\n" +
            "   ClientName\r\n");

        if (Accounts != -1) {
            for (var zz = 0; zz < Accounts.Row.length; zz++) {
                pBankAcc.AddGridRow("BankAccount", Accounts.Row[zz].Account_No + "|" + Accounts.Row[zz].Payee + "|" + Accounts.Row[zz].Bank + "|" + Accounts.Row[zz].Region)
            }
        }
    }
    if (p1.GetValue("Payment") == "Link Payment") {
        var Accounts = sql.Fill("SELECT\r\n" +
            "	DISTINCT\r\n" +
            "	UserAccountCode [Account_No],\r\n" +
            "	ClientName [Payee],\r\n" +
            "	CheqAccCode [Bank],\r\n" +
            "	KapptiCode [Region]  \r\n" +
            "from \r\n" +
            "	SLAccount \r\n" +
            "where \r\n" +
            "	GLAccCode = \'Extern Chq\'\r\n" +
            "	and CheqAccCode not like \'%ABSA%\'\r\n" +
            "	and isnull(Obsolete, 0 ) = 0\r\n" +
            "	and CheqAccCode <> \'\'\r\n" +
            "	and Clasification = \'COMM\'\r\n" +
            "order by\r\n" +
            "   ClientName\r\n");


        if (Accounts != -1) {
            for (var zz = 0; zz < Accounts.Row.length; zz++) {
                pBankAcc.AddGridRow("BankAccount", Accounts.Row[zz].Account_No + "|" + Accounts.Row[zz].Payee + "|" + Accounts.Row[zz].Bank + "|" + Accounts.Row[zz].Region)
            }
        }
    }
}
function pBankAcc_Exit() {
    if (p1.GetValue("Payment") != "") {
        if (pBankAcc.GetSelectedRows("BankAccount") == "") {
            pBankAcc.AddInvalidMessage("Please select a payment account before going to the next p.")
        }
    }
}
function CreateImportFile() {
    //var filePath = "\\\\XZAPBCC1APPA660\\mentis_data\\Dynafile\\DeliveredStockCalc.xlsx"
    var filePath = "\\\\xzaobcc2app0572\\DYNAFILE_UAT\\Temp\\DeliveredStockCalc.xlsx"
    var excel = new ActiveXObject("Excel.Application")
    var workbook = excel.Workbooks.Open(filePath)
    var sheet = workbook.Sheets(1);

    var User = sql.GetScalar("Select SUBSTRING(SYSTEM_USER, CHARINDEX(\'\\\', SYSTEM_USER)+1 , LEN(SYSTEM_USER)) as \'CurrentUser\'")

    workbook.SaveAs("C:\\Users\\" + User + "\\Desktop\\Delivered Stock Calc.xlsx");
    excel.Visible = true;
}
function CalcTpe() {
    if (p1.GetValue("CalcTpe") == "COFCO Calculation") {

        GlobalVar.FormName = "pCOFCO"

        p1.SetEnabled("Statement", false)
        p1.SetEnabled("Payment", false)
        p1.SetValue("Statement", "No Statement")
        p1.SetValue("Payment", "No Payment")
    }
    else {
        GlobalVar.FormName = "SdtCalc"

        p1.SetEnabled("Statement", true)
        p1.SetEnabled("Payment", true)
    }
}
function NextPage() {
    var TotalExclVat = ""
    var VatTotal = ""
    var TotalIncVat = ""
    var Grid = ""

    cntx = "ONE"
    if (p1.GetValue("CalcTpe") == "COFCO Calculation" && CofcoPage.Value == 1) {
        p1.NextPage = pCOFCO
        TotalExclVat = "Total Excl Price Adjustment"
        VatTotal = "Price Adjustment Total"
        TotalIncVat = "Total Incl Price Adjustment"

        var SelectedDate = SQLDate(new Date(p1.getValue("TransDate")), 1)

        var today = sql.Fill("SELECT DATEADD(day, 14, \'" + SelectedDate + "\') [Days]")

        today = new Date(today.Row[0].Days);
        today = sql.SQLDate(today, 2);

        pCOFCO.SetValue("EndDate", today)

        p3.AddField("ChqLocal", "Chq Local Inc PA", "text", "readonly=true");
        p3.AddField("ValTClient", "Value due to client (EXCL PA)", "text", "readonly=true");
        p3.AddField("TotalIncVat", TotalIncVat, "text", "readonly=true");
        p3.AddField("TotalExclVat", TotalExclVat, "text", "readonly=true");
        p3.AddField("VatTotal", VatTotal, "text", "readonly=true");
        p3.AddField("TotalTon", "Total Units:", "text", "readonly=true");
        p3.AddField("AdminFees", "Admin Fees per Unit:", "text", "readonly=true");

        p3.SetGridLayout("TotalIncVat", 2, 0, 1, 1);
        p3.SetGridLayout("ValTClient", 2, 1, 1, 1);
        p3.SetGridLayout("ChqLocal", 3, 0, 1, 1);
        p3.SetGridLayout("TotalExclVat", 1, 0, 1, 1);
        p3.SetGridLayout("VatTotal", 1, 1, 1, 1);
        p3.SetGridLayout("TotalTon", 0, 0, 1, 1);
        p3.SetGridLayout("AdminFees", 0, 1, 1, 1);

        CofcoPage.Value = 2

    }
    if (p1.GetValue("CalcTpe") == "Standard Delivered Calculation" && StandardPage.Value == 1) {
        GetPaymentAccount()
        TotalExclVat = "Total Excl Vat"
        VatTotal = "Vat Total"
        TotalIncVat = "Total Inc Vat"

        p3.AddField("TotalIncVat", TotalIncVat, "text", "readonly=true");
        p3.AddField("TotalExclVat", TotalExclVat, "text", "readonly=true");
        p3.AddField("VatTotal", VatTotal, "text", "readonly=true");
        p3.AddField("TotalTon", "Total Units:", "text", "readonly=true");
        p3.AddField("AdminFees", "Admin Fees per Unit:", "text", "readonly=true");

        p3.SetGridLayout("TotalIncVat", 1, 2, 1, 1);

        p3.SetGridLayout("TotalExclVat", 1, 0, 1, 1);
        p3.SetGridLayout("VatTotal", 1, 1, 1, 1);
        p3.SetGridLayout("TotalTon", 0, 0, 1, 1);
        p3.SetGridLayout("AdminFees", 0, 1, 1, 1)

        StandardPage.Value = 2
    }
}
// function SdtCalcNextP() {
//     // if (GlobalVar.FSAvalue == "") {
//     //     SdtCalc.AddInvalidMessage("Please Enter an FSA Number.")
//     // }
//     // if (SdtCalc.GetValue("Ton") != "" && SdtCalc.GetValue("InvoiceNo") != "" && SdtCalc.GetValue("InvAmount") != "" && SdtCalc.GetValue("EndDate") != null) {
//     //     SdtCalc.AddInvalidMessage("Please add the stock line before moving on to the final p");
//     // }
//     // SdtCalc.NextPage = p3
// }
function COFCOCalcNextP() {
    if (GlobalVar.FSAvalue == "") {
        SdtCalc.AddInvalidMessage("Please Enter an FSA Number.")
    }
    if (pCOFCO.GetValue("Ton") != "" && pCOFCO.GetValue("InvoiceNo") != "" && pCOFCO.GetValue("InvAmount") != "" && pCOFCO.GetValue("EndDate") != null) {
        pCOFCO.AddInvalidMessage("Please add the stock line before moving on to the final p");
    }
    pCOFCO.NextPage = p3
}
function DeleteCalculation() {
    cntx = "Delete Calc"
    var discountCalcArray = p0.GetSelectedRows("DiscCalcs").split(";");
    var object = sql.GetScalar("Select ObjectNo From [Object] Where Code = \'StorageData\'");

    if (p0.GetSelectedRows("DiscCalcs").length != 0) {
        if (Popup("System Message", "Are you sure you want to delete this Discount calculation?")) {
            var dataNo = discountCalcArray[0].split("|")[1];
            DeleteReserveDebtors("DataNo", dataNo);
            PFECalcTrigger(1, ChildInstanceRefNo.toString(), ChildInstanceRefNo.toString(), true);
            sql.conn.Execute("UPDATE dbo.ObjectChangeRequest SET Status = 2 , RequesterUserID = \'" + Userid.Userid + "\', Difference = \'Calculation deleted by user in Create macro\' WHERE InstanceRefNo = \'" + dataNo + "\' and ObjectNo = 47\r\n")
            p0.ClearGridRows("DiscCalcs");
            GetDiscountCalcs()

        }
    }
    else {
        JSMsg("Please select a discount calculation before deleting", "Error")
    }

}
function trim(Cert) {
    var CertNo = sql.GetScalar("SELECT RTRIM(LTRIM(\'" + Cert + "\')) [Cert]\r\n")
    return CertNo
}
function LoadTotals() {
    // cntx = "LoadTotals"
    // if (p1.GetValue("CalcTpe") == "Standard Delivered Calculation") {
    //     var TotalTon = 0;
    //     var AdminFee = 0;
    //     var AmountExclVat = 0;
    //     var VatTotal = 0;
    //     var TotalInclVat = 0;
    //     var TotInvAmount = 0
    //     var getTotals = SdtCalc.GetValue("StockDet").split(";");

    //     for (a = 0; a < getTotals.length; a++) {
    //     //     SdtCalc.AddField("StockDet", "Stock Details", "Grid", "columns=Project Code(type:string,width:80,readonly:true)|Invoice No(type:string,width:80,readonly:true)|End Date(type:string,width:80,readonly:true)|Days(type:string,width:100,readonly:true)|Inv Amount(type:string,width:80,readonly:true)|Interest(type:string,width:100,readonly:true)"
    //     // + "|Total Admin Fee(type:string,width:120,readonly:true)|Tons(type:string,width:80,readonly:true)|Value Incl VAT(type:string,width:120,readonly:true)|Amount Excl VAT(type:String,width:120,readonly:true)|VAT Amount(type:string,width:80,readonly:true)|FSA Number(type:String,width:120,readonly:true)|Measurement(type:string,width:100,readonly:true);enabledelete=true");
    //         //Project Code   Invoice No   End Date   Days   Inv Amount   Interest   Total Admin Fee   Tons   Value Incl VAT   Amount Excl VAT    VAT Amount
    //         TotalTon += new Number(getTotals[a].split("|")[7]);
    //         AdminFee += new Number(getTotals[a].split("|")[6]);
    //         AmountExclVat += new Number(getTotals[a].split("|")[9]);
    //         VatTotal += new Number(getTotals[a].split("|")[10]);
    //         TotalInclVat += new Number(getTotals[a].split("|")[8]);
    //         TotInvAmount += new Number(getTotals[a].split("|")[4]);

    //         var AdminFee = Math.round(AdminFee * 100) / 100;
    //         var AmountExclVat = Math.round(AmountExclVat * 100) / 100;
    //         var VatTotal = Math.round(VatTotal * 100) / 100;
    //         var TotalInclVat = Math.round(TotalInclVat * 100) / 100;
    //         var TotInvAmount = Math.round(TotInvAmount * 100) / 100;

    //     }

    //     p3.SetValue("TotalExclVat", AmountExclVat);
    //     p3.SetValue("VatTotal", VatTotal);
    //     p3.SetValue("TotalIncVat", TotalInclVat);
    //     p3.SetValue("TotalTon", TotalTon);
    //     p3.SetValue("AdminFees", AdminFee);
    //}
    //else {
        var TotalTon = 0;
        var AdminFee = 0;
        var AmountExclPriceAdjust = 0;
        var PriceAdjustTotal = 0;
        var TotalInclPriceAdjust = 0;
        var ValDueToClient = 0;
        var ChqLocal = 0

        var getTotals = pCOFCO.GetValue("StockDet").split(";");

        for (a = 0; a < getTotals.length; a++) {

            TotalTon += new Number(getTotals[a].split("|")[8]);
            AdminFee += new Number(getTotals[a].split("|")[7]);
            AmountExclPriceAdjust += new Number(getTotals[a].split("|")[3]);
            PriceAdjustTotal += new Number(getTotals[a].split("|")[4]);
            TotalInclPriceAdjust += new Number(getTotals[a].split("|")[5]);
            ValDueToClient += new Number(getTotals[a].split("|")[9]);
            ChqLocal += new Number(getTotals[a].split("|")[10]);

            AdminFee = Math.round(AdminFee * 100) / 100;
            AmountExclPriceAdjust = Math.round(AmountExclPriceAdjust * 100) / 100;
            PriceAdjustTotal = Math.round(PriceAdjustTotal * 100) / 100;
            TotalInclPriceAdjust = Math.round(TotalInclPriceAdjust * 100) / 100;
            ValDueToClient = Math.round(ValDueToClient * 100) / 100;
            ChqLocal = Math.round(ChqLocal * 100) / 100;

        }

        p3.SetValue("TotalExclVat", AmountExclPriceAdjust);
        p3.SetValue("VatTotal", PriceAdjustTotal);
        p3.SetValue("TotalIncVat", TotalInclPriceAdjust);
        p3.SetValue("TotalTon", TotalTon);
        p3.SetValue("AdminFees", AdminFee);
        p3.SetValue("ChqLocal", ChqLocal);
        p3.SetValue("ValTClient", ValDueToClient);
   // }
}
function SaveXML() {
    try {
        sql.conn.BeginTrans();
        cntx = "Save Discount Calc";
        var Manualcalc = "Manual Calculation";
        var TpeCalc = "Delivered Stock"
        if (p1.GetValue("CalcTpe") == "COFCO Calculation") {
            TpeCalc = "COFCO Delivered Stock"
        }

        var schemaNo = sql.GetScalar("Select SchemaNo From StorageSchema Where Description = \'Discount Calculation\'");//10
        so.Load(schemaNo);
        var sd = so.CreateStorageData();
        sd.ObjectNo = 1;
        sd.InstanceRefNo = ChildInstanceRefNo.toString();
        sd.Description = Manualcalc + " " + TpeCalc
        sd.Tag = p1.GetValue("TypeComm")
        sd.RefNo = false + "|";
        sd.DataXML = BuildXML();
        sd.DT_CreateDate = sql.SQLDate(new Date(), 1);

        var changeXML = BuildChangeXML();

        cntx = "Create Change Request";
        var objNo = sql.GetScalar("Select ObjectNo From [Object] where Code = \'StorageData\'");//47

        if (p0.GetValue("Action") == "AMEND EXISTING") {
            var calcArray = p0.GetSelectedRows("DiscCalcs").split(";");
            sd.DataNo = calcArray[0].split("|")[1];
            sd.Update();
            var changeRequest = sql.Fill("Select * From ObjectChangeRequest Where ObjectNo = " + objNo + " and InstanceRefNo = " + sd.DataNo);
            sql.conn.Execute("Update ObjectChangeRequest set RequesterUserId = \'" + Userid.Userid + "\', Action = 0, DT_CreateDate = GetDate(), Content = \'" + changeXML + "\' Where ObjectChangeRequestNo = " + changeRequest.Row[0].ObjectChangeRequestNo);
            PFECalcTrigger(objNo, sd.DataNo, ChildInstanceRefNo.toString(), true);
        }
        else {
            sd.Insert();
            var ocrNo = sql.GetScalar("Select Max(ObjectChangeRequestNo)+1 From ObjectChangeRequest");
            sql.conn.Execute("Insert into ObjectChangeRequest Values(" + ocrNo + ", " + objNo + ", " + sd.DataNo + ", \'Manual Calc Delivered Stock\', 0, 0, \'" + Userid.Userid + "\', getDate(), \'" + changeXML + "\', null, null, \'\',NULL)");
            PFECalcTrigger(objNo, sd.DataNo, ChildInstanceRefNo.toString(), true);
        }
        sql.conn.CommitTrans();
    }
    catch (ex) {
        sql.conn.RollbackTrans();
        JSMsg(ex.message, cntx);
    }
}
function ReserveDebtors(project, InvAmount) {
    cntx = "ReserverDebtors"
    try {

        if (p0.GetValue("Action") != "AMEND EXISTING") {


            var ReserveNO = sql.GetScalar("INSERT INTO ReservedStock(ObjectNo,InstanceRefNo,ContractGradeNo,StockQuantity,Amount,GLAccCode,rowguid)\r\n" +
                "OUTPUT Inserted.ID\r\n" +
                "VALUES((SELECT ObjectNo FROM Object WHERE Caption = \'Project\')," + project + ",0,0," + InvAmount + ",860,NEWID())\r\n");
            return ReserveNO;

        }
        else {

            var res = sql.GetScalar("SELECT InstanceRefNo FROM ReservedStock WHERE ID = " + ReserveID.value + "\r\n")
            if (res == 0) {
                var ReserveNO = sql.GetScalar("INSERT INTO ReservedStock(ObjectNo,InstanceRefNo,ContractGradeNo,StockQuantity,Amount,GLAccCode,rowguid)\r\n" +
                    "OUTPUT Inserted.ID\r\n" +
                    "VALUES((SELECT ObjectNo FROM Object WHERE Caption = \'Project\')," + project + ",0,0," + InvAmount + ",860,NEWID())\r\n");
                return ReserveNO;
            }
            else {

                sql.conn.Execute("UPDATE ReservedStock SET InstanceRefNo = " + project + " , Amount = " + InvAmount + "\r\n" +
                    "WHERE ID = " + ReserveID.value + "\r\n");
                return ReserveID.value;
            }

        }
    } catch (error) {
        JSMsg(error.message, cntx);
    }

}
function LoadNextPage() {

    var availible = new Number(p4.GetValue("Available").replace(/,/g, ""));
    if (availible > 0) {
        p4.BlankNextPage();
        p4.ExitScript = CreateDoc;
    }
    else {
        p4.NextPage = p5;
        p5.ExitScript = UploadDoc;
        p5.BlankNextPage();
    }
}
function CreateDoc() {
    try {
        cntx = "Create Document";
        var oShell = new ActiveXObject("Shell.Application");
        var fso = new ActiveXObject("Scripting.FileSystemObject");

        var discountCalcArray = p0.GetSelectedRows("DiscCalcs").split(";");
        var dataNo = discountCalcArray[0].split("|")[1];
        Root = readXML(dataNo);
        ini.loadini("ConfigIni", "C:\\Mentis\\config.ini");
        if (ini.error.code == 1)
            throw ini.error

        if (Root.selectSingleNode("//Root/TransType").text == "Standard Delivered Calculation") {
            //var filePath = "\\\\XZAPBCC1APPA660\\mentis_data\\Dynafile\\DeliveredStockTemp.xlsx"
            //sql.GetScalar("SELECT FileName from Document WHERE RefNo = \'Template\' and Type = \'Manual\' ANd Subject = \'Deliverd Stock Calc\'");

            var filePath = "\\\\xzaobcc2app0572\\DYNAFILE_UAT\\Discount Calculations\\Manual11676827.xlsx"
            var excel = new ActiveXObject("Excel.Application");
            var workbook = excel.Workbooks.Open(filePath);
            var sheet = workbook.Sheets(1);
            workbook.SaveAs("C:\\Mentis\\TempDC.xlsx");

            sheet.Cells(10, 2).Value = p0.GetValue("dProject");
            sheet.Cells(11, 2).Value = Root.selectSingleNode("//Root/ClientName").text
            sheet.Cells(13, 2).Value = Root.selectSingleNode("//Root/TransDate").text;
            sheet.Cells(14, 2).Value = Root.selectSingleNode("//Root/PaymentToll").text;
            sheet.Cells(15, 2).Value = Root.selectSingleNode("//Root/ActualInt").text;
            sheet.Cells(16, 2).Value = Root.selectSingleNode("//Root/AdminFeePTon").text
            sheet.Cells(15, 7).Value = Root.selectSingleNode("//Root/AmountExclVat").text
            sheet.Cells(16, 7).Value = Root.selectSingleNode("//Root/VatTotal").text
            sheet.Cells(17, 7).Value = Root.selectSingleNode("//Root/TotalInclVat").text
            sheet.Cells(18, 7).Value = Root.selectSingleNode("//Root/TotalTon").text
            sheet.Cells(21, 7).Value = "Admin Fee R" + Root.selectSingleNode("//Root/AdminFeePTon").text + " per Unit"

            cntx = "Formulas"
            sheet.Cells(7, 6).value = "Formulas:";
            sheet.Cells(8, 6).value = "Interest:";
            sheet.Cells(8, 7).value = "Invoice amount x Interest Rate x (Financed Days / 365):";
            sheet.Cells(9, 6).value = "Admin Fee:";
            sheet.Cells(9, 7).value = "Admin Fee x Tonnage x Vat";
            sheet.Cells(10, 6).value = "Value inc Vat:";
            sheet.Cells(10, 7).Value = "Invoice amount - Interest - Admin Fee";
            sheet.Cells(11, 6).value = "Value excl Vat:";
            sheet.Cells(11, 7).value = "Value incl Vat x 100 ) / (100 + Vat)";
            sheet.Cells(12, 6).value = "Vat Amount:";
            sheet.Cells(12, 7).value = "Amount excl Vat x (Vat/100)";

            var offsetLine = 22;
            var Stock = Root.selectNodes("Stock");
            for (var s = 0; s < Stock.length; s++) {
                StockItem = Stock.item(s);
                var StockInvoiceNo = StockItem.getAttribute("StockInvoiceNo");

                sheet.Cells(offsetLine, 1).Value = StockInvoiceNo;
                sheet.Cells(offsetLine, 2).Value = Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/EndDate").text;
                sheet.Cells(offsetLine, 3).Value = Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/DaysFinanced").text;
                sheet.Cells(offsetLine, 4).Value = Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/InvAmount").text
                sheet.Cells(offsetLine, 5).Value = Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/Interest").text;
                sheet.Cells(offsetLine, 6).Value = Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/AdminFee").text;
                sheet.Cells(offsetLine, 7).Value = Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/Tonage").text;
                sheet.Cells(offsetLine, 8).Value = Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/ValueIncVat").text;
                sheet.Cells(offsetLine, 9).Value = Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/ValueExclVat").text;
                sheet.Cells(offsetLine, 10).Value = Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/VatAmount").text;
                sheet.Cells(offsetLine, 11).Value = Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/FSANumber").text;


                offsetLine++
            }
            //AddFSASummary(offsetLine + 1, p0.GetValue("dProject"), sheet);
            
            if(p0.getValue("invoice") == true)
            {
                AddAddressDetails(ChildInstanceRefNo, sheet, sql, dataNo, 7);
            }
        }
        else {
            //var filePath = "\\\\XZAPBCC1APPA660\\mentis_data\\Dynafile\\COFCOCalcTemp.xlsx"
            // sql.GetScalar("SELECT FileName from Document WHERE RefNo = \'Template\' and Type = \'Manual\' ANd Subject = \'Deliverd COFCOCalc\'");

            var filePath = "\\\\xzaobcc2app0572\\DYNAFILE_UAT\\Discount Calculations\\Manual11676828.xlsx"
            var excel = new ActiveXObject("Excel.Application");
            var workbook = excel.Workbooks.Open(filePath);
            var sheet = workbook.Sheets(1);

            workbook.SaveAs("C:\\Mentis\\TempDC.xlsx");

            sheet.Cells(10, 2).Value = p0.GetValue("dProject");
            sheet.Cells(11, 2).Value = Root.selectSingleNode("//Root/ClientName").text
            sheet.Cells(13, 2).Value = Root.selectSingleNode("//Root/TransDate").text;
            sheet.Cells(14, 2).Value = Root.selectSingleNode("//Root/PaymentToll").text;
            sheet.Cells(15, 2).Value = Root.selectSingleNode("//Root/ActualInt").text;
            sheet.Cells(13, 7).Value = Root.selectSingleNode("//Root/TotalTon").text
            sheet.Cells(14, 7).Value = Root.selectSingleNode("//Root/AdminFeePTon").text
            sheet.Cells(15, 7).Value = Root.selectSingleNode("//Root/AmountExclPriceAdjust").text
            sheet.Cells(16, 7).Value = Root.selectSingleNode("//Root/PriceAdjustTotal").text
            sheet.Cells(17, 7).Value = Root.selectSingleNode("//Root/TotalInclPriceAdjust").text
            sheet.Cells(18, 7).Value = Root.selectSingleNode("//Root/ValDueToClient").text
            sheet.Cells(19, 7).Value = Root.selectSingleNode("//Root/ChqLocal").text


            var Stock = Root.selectNodes("Stock");
            var offsetLine = 22;
            for (var s = 0; s < Stock.length; s++) {
                StockItem = Stock.item(s);
                var StockInvoiceNo = StockItem.getAttribute("StockInvoiceNo");

                sheet.Cells(offsetLine, 1).Value = StockInvoiceNo;
                sheet.Cells(offsetLine, 2).Value = Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/EndDate").text;
                sheet.Cells(offsetLine, 3).Value = Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/DaysFinanced").text;
                sheet.Cells(offsetLine, 4).Value = Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/InvAmount").text
                sheet.Cells(offsetLine, 5).Value = Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/PriceAdjust").text;
                sheet.Cells(offsetLine, 6).Value = Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/AmtExclPA").text;
                sheet.Cells(offsetLine, 7).Value = Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/Interest").text;
                sheet.Cells(offsetLine, 8).Value = Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/AdminFee").text;
                sheet.Cells(offsetLine, 9).Value = Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/Tonage").text;
                sheet.Cells(offsetLine, 10).Value = Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/ValToClient").text;
                sheet.Cells(offsetLine, 11).Value = Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/PriceAdjust").text;
                sheet.Cells(offsetLine, 12).Value = Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/ChqLocalAmt").text;
                sheet.Cells(offsetLine, 13).Value = Root.selectSingleNode("Stock[@StockInvoiceNo=\'" + StockInvoiceNo + "\']/FSANumber").text;
                offsetLine++
            }
            //AddFSASummary(offsetLine + 1, p0.GetValue("dProject"), sheet);

            if(p0.getValue("invoice") == true)
            {
                AddAddressDetails(ChildInstanceRefNo, sheet, sql, dataNo, 3);
            }
        }
        
        workbook.SaveAs("C:\\Mentis\\TempDC2.xlsx");
        workbook.ExportAsFixedFormat(0, "C:\\Mentis\\TempDC2.pdf");

        workbook.Close(true);
        excel.Quit()

        var f = fso.GetFile("C:\\Mentis\\TempDC.xlsx");
        var f2 = fso.GetFile("C:\\Mentis\\TempDC2.xlsx");
        f.Delete();
        f2.Delete();
        if (ManualCalc.value == false) {
            CreatePFECalcDoc(p4);
        }
        var objNo = sql.GetScalar("Select ObjectNo From [Object] Where Code = \'StorageData\'");
        var changeRequest = sql.Fill("Select * From ObjectChangeRequest Where ObjectNo = " + objNo + " and InstanceRefNo = " + dataNo);
        sql.conn.Execute("Update ObjectChangeRequest set RequesterUserId = \'" + Userid.Userid + "\', DT_CreateDate = GetDate(), Action = 1 Where ObjectChangeRequestNo = " + changeRequest.Row[0].ObjectChangeRequestNo);
        StoreResult("Complete", "Discount Calc Complete");
        oShell.ShellExecute("C:\\Mentis\\TempDC2.pdf", "", "", "open", "1");

        r = pm.AddActivity("Finance calculation", "Completed Milldoor Discount Calc ", "Prepared a Milldoor Discount Calc", ChildInstanceRefNo, 1);

    }
    catch (ex) {
        r = pm.AddActivity("Finance calculation", "Failed Milldoor Discount Calc " + ex.message, "Prepared a Milldoor Discount Calc", ChildInstanceRefNo, 1);

        try {
            workbook.Close();
        } catch (error) { }

        try {
            excel.Quit();
        } catch (error) { }
    }

}
function AddAddressDetails(ProjectCode, sheet, SQLo, dataNo, headerPosition){
    var bank = SQLo.Fill("SELECT \r\n"+
                        "	A.Name,\r\n"+
                        "	MS.Line [Address],\r\n"+
                        "	A.VATRegNo,\r\n"+
                        "	A.RegistrationNo\r\n"+
                        "FROM \r\n"+
                        "	Address A\r\n"+
                        "	JOIN MemoString MS ON MS.MemoNo = A.PhysicalAddress\r\n"+
                        "WHERE \r\n"+
                        "	A.AddressNo = (SELECT A.AddressNo \r\n"+
                        "					FROM [Address] A \r\n"+
                        "						JOIN AccountCompany AC ON AC.AccCompNo = A.RefNo  \r\n"+
                        "					WHERE AC.ProjectCode = 0 AND A.RefCode = \'AC\' AND A.DefaultAddress = 1)\r\n");

    var client= SQLo.Fill("SELECT \r\n"+
                        "	A.Name,\r\n"+
                        "	MS.Line [Address],\r\n"+
                        "	A.VATRegNo,\r\n"+
                        "	A.RegistrationNo\r\n"+
                        "FROM \r\n"+
                        "	Address A\r\n"+
                        "	JOIN MemoString MS ON MS.MemoNo = A.PhysicalAddress\r\n"+
                        "WHERE \r\n"+
                        "	A.AddressNo = (SELECT A.AddressNo \r\n"+
                        "					FROM [Address] A\r\n"+
                        "						JOIN AccountCompany AC ON AC.AccCompNo = A.RefNo \r\n"+
                        "					WHERE AC.ProjectCode = \'"+ProjectCode+"\' AND A.RefCode = \'AC\' AND A.ObsoleteFlag != 1 AND A.DefaultAddress = 1)\r\n");

    if(bank != -1 && client != -1){        

        sheet.Cells(headerPosition,1).Value = "Invoice for Commodity Supply";
        sheet.Cells(headerPosition,9).Value = "Invoice Number:"
        sheet.Cells(headerPosition,10).Value = dataNo;
        
        sheet.Cells(15,12).Value = "Recipient:"

        sheet.Cells(16,12).Value = "Name:";
        sheet.Cells(17,12).Value = "VAT Reg No:";
        sheet.Cells(18,12).Value = "Reg No:";
		sheet.Cells(19,12).Value = "Address:";

        sheet.Cells(16,13).Value = bank.Row[0].Name;
        sheet.Cells(17,13).Value = bank.Row[0].VATRegNo;
        sheet.Cells(18,13).Value = bank.Row[0].RegistrationNo;
		sheet.Cells(19,13).Value = remove_linebreaks(bank.Row[0].Address);
        
        sheet.Cells(15,9).Value = "Supplier:";

        sheet.Cells(16,9).Value = "Name:";
        sheet.Cells(17,9).Value = "VAT Reg No:";
        sheet.Cells(18,9).Value = "Reg No:";
        sheet.Cells(19,9).Value = "Address:";

		sheet.Cells(16,10).Value = client.Row[0].Name;
        sheet.Cells(17,10).Value = client.Row[0].VATRegNo;
        sheet.Cells(18,10).Value = client.Row[0].RegistrationNo;
        sheet.Cells(19,10).Value = remove_linebreaks(client.Row[0].Address);

        sheet.Range("J16:K19").NumberFormat = "General";
        sheet.Range("M16:N19").NumberFormat = "General";

        sheet.Range("J16:K19").VerticalAlignment = 1;
		sheet.Range("M16:N19").VerticalAlignment = 1;

        sheet.Rows("19:19").RowHeight = "75";

        sheet.Range("J15:J15").Font.Bold = true;
        sheet.Range("M15:M15").Font.Bold = true;

        //sheet.Range("J5:K5").Font.Bold = true;
        with(sheet.Range("J" + headerPosition + ":K" + headerPosition).Font){
            Bold = true;
            Size = 14;
        }

        sheet.Columns.Autofit;
        
        var xlEdgeBottom = 9;
        var xlSlantDashDot = 13;
        var xlThick = 2;
        var xlAutomatic = -4105;

        var range = sheet.Cells(15,9);
        with (range.Borders(xlEdgeBottom)) {
            LineStyle = xlSlantDashDot;
            Weight = xlThick;
            ColorIndex = xlAutomatic;
        }
        var range = sheet.Cells(15,10);
        with (range.Borders(xlEdgeBottom)) {
            LineStyle = xlSlantDashDot;
            Weight = xlThick;
            ColorIndex = xlAutomatic;
        }
        var range = sheet.Cells(15,12);
        with (range.Borders(xlEdgeBottom)) {
            LineStyle = xlSlantDashDot;
            Weight = xlThick;
            ColorIndex = xlAutomatic;
        }
        var range = sheet.Cells(15,13);
        with (range.Borders(xlEdgeBottom)) {
            LineStyle = xlSlantDashDot;
            Weight = xlThick;
            ColorIndex = xlAutomatic;
        }   
    }
    function remove_linebreaks(str) { 
		return str.replace( /[\r\n]+/gm, "\n" );
	}
}

function UploadDoc() {
    if (ManualCalc.value == true) {
        var file = p6.GetValue("PFEFile");
        UploadDocData(p6, "Manual PFE", file)
    }
    else {
        var file = p5.GetValue("File");
        UploadDocData(p5, "Credit approval for limit", file)
    }
}
function ManaulStatus() {

    try {
        cntx = "Set status to manual upload"
        var dataNo = p0.GetSelectedRows("DiscCalcs").split(";")[0].split("|")[1];
        var object = sql.GetScalar("Select ObjectNo From [Object] Where Code = \'StorageData\'")
        sql.conn.Execute("UPDATE MessageQueue SET Status = 4 WHERE MessageQueueNo = (SELECT TOP 1 MessageQueueNo FROM MessageQueue WHERE Status = 3 OR Status = 4 AND ObjectNo = " + object + " AND InstanceRefNo = \'" + dataNo + "\' ORDER By CreateDate DESC)")
        UploadDoc();
    }
    catch (ex) {
        JSMsg(ex.message, cntx)
    }
}
function UploadDocData(ppage, psubject, file) {
    try {
        cntx = "Save documents"
        var dataNo = p0.GetSelectedRows("DiscCalcs").split(";")[0].split("|")[1];

        if (file == "") {
            ppage.AddInvalidMessage("Please select a file to upload");
        }
        else {

            var NoteID = GetNewNoteID();
            var fso = new ActiveXObject("Scripting.FileSystemObject");
            var myDocument = {
                Type: "StorageData",
                Code: dataNo,
                Person: Userid.Userid,
                Subject: psubject,
                RefNo: dataNo,
                Category: sql.GetScalar("Select CategoryNo From Category where Usage = \'DOC\' and Description like \'%B - Discount Calculation%\' and WhatProjects = 1")
            }

            var newfile = DynaFilepath.value + "\\" + myDocument.Type + myDocument.Code + NoteID + file.substr((file.lastIndexOf('.')))

            sql.conn.Execute("INSERT INTO Document\r\n" +
                "([NoteID], [Type], [Code], [Person], [Subject], [RefNo] , [Category], [FileName], [UploadedDate], [rowguid])\r\n" +
                "VALUES\r\n" +
                "(\r\n" +
                "	" + NoteID + ",\r\n" +
                "	\'" + myDocument.Type + "\',\r\n" +
                "	\'" + myDocument.Code + "\',\r\n" +
                "	\'" + myDocument.Person + "\',\r\n" +
                "	\'" + myDocument.Subject + "\',\r\n" +
                "	\'" + myDocument.RefNo + "\',\r\n" +
                "     " + myDocument.Category + ",\r\n" +
                "	\'" + newfile + "\',\r\n" +
                "   GETDATE(),   \r\n" +
                "   NEWID()\r\n)")

            fso.Copyfile(file, newfile);
            CreateDoc();
        }
    }
    catch (ex) {
        JSMsg(ex.message, cntx)
    }

}
//#region Add FSA Summary AW Added
//The last line parameter is a number of where the last line in the excel is. 
//The FSA Summary will start saving the format below the last line in the Excel.
function AddFSASummary(lastline, projectCode, sheet1) {
    sheet1.cells(lastline, 1).Value = "FSA Summary:";
    sheet1.Range("A" + lastline + ":A" + (lastline)).Font.Bold = "True";
    sheet1.Range("A" + (lastline + 1) + ":G" + (lastline)).Borders(8).LineStyle = 1;
    sheet1.Range("A" + (lastline + 1) + ":G" + (lastline)).Borders(8).Weight = 2;

    var GetContracts = sql.Fill("SELECT	CT.ContractNo, \r\n" +
        "		CT.ProjectCode,\r\n" +
        "		P.ProjectDescription, \r\n" +
        "		CT.[Description][Contract],\r\n" +
        "		CT.AdjustedQuantity[TotalTonnage],\r\n" +
        "		ISNULL(CONVERT(VARCHAR(10),DT_AllocationDate,120),\'\') +\' TO \'+ISNULL(CONVERT(VARCHAR(10),CASE WHEN CT.DT_ExtensionDate IS NULL THEN CT.DT_ExpiryDate ELSE CT.DT_ExtensionDate END,120),\'\') [Deliveryperiod]\r\n" +
        "FROM \r\n" +
        "		Contract CT\r\n" +
        "		JOIN ProjectA P on P.ProjectCode = CT.ProjectCode\r\n" +
        "WHERE	\r\n" +
        "		CT.ProjectCode = " + projectCode + " \r\n" +
        "		AND CT.[Status] = 1\r\n" +
        "		AND CT.InactiveFlag <> 1 \r\n" +
        "		AND CT.ContractTypeNo = 19\r\n" +
        "GROUP BY \r\n" +
        "		CT.ContractNo,\r\n" +
        "		CT.ProjectCode,\r\n" +
        "		P.ProjectDescription,\r\n" +
        "		CT.AdjustedQuantity,\r\n" +
        "		CT.[Description],\r\n" +
        "		CT.DT_ExpiryDate,\r\n" +
        "		CT.DT_ExtensionDate,\r\n" +
        "		DT_AllocationDate\r\n" +
        "ORDER BY \r\n" +
        "		CT.ContractNo ASC\r\n");

    if (GetContracts != -1) {
        cntx = "Start creating FSA report"
        sheet1.cells(lastline, 1).Value = GetContracts.Row[0].ProjectDescription;
        sheet1.cells(lastline + 1, 1).Value = "Project " + projectCode;
        sheet1.Range("A" + lastline + ":A" + (lastline + 1)).Font.Bold = "True";

        sheet1.cells(lastline + 3, 1).Value = "Contract";
        sheet1.cells(lastline + 3, 2).Value = "Grade";
        sheet1.cells(lastline + 3, 3).Value = "Total Tonnage";
        sheet1.cells(lastline + 3, 4).Value = "Finance";
        sheet1.cells(lastline + 3, 5).Value = "Left to Finance";
        sheet1.cells(lastline + 3, 6).Value = "Delivery Period";
        sheet1.cells(lastline + 3, 7).Value = "Long Positions";
        sheet1.Range("A" + (lastline + 3) + ":G" + (lastline + 3)).Font.Bold = "True";
        sheet1.Range("A" + (lastline + 4) + ":G" + (lastline + 4)).Borders(8).LineStyle = 1;
        sheet1.Range("A" + (lastline + 4) + ":G" + (lastline + 4)).Borders(8).Weight = 2;

        //Set the row number to begin at row 5
        lastline += 4;
        for (count = 0; count < GetContracts.Row.length; count++) {
            //Populate the header of each contract
            sheet1.cells(lastline, 1).Value = GetContracts.Row[count].Contract;
            sheet1.cells(lastline, 1).Font.Bold = "True";
            sheet1.cells(lastline, 6).Value = GetContracts.Row[count].Deliveryperiod;
            //Get the long positions tonnage and minus it from the total stock
            var long = sql.Fill("SELECT SUM(StockQuantity)[StockQuantity],SUM(Positions)[Positions] from ContractLongPos\r\n" +
                "                                WHERE ContractNo = " + GetContracts.Row[count].ContractNo + " AND ProjectCode = " + projectCode + " AND CAST(DT_Create AS date)  = CAST(getdate() AS date) \r\n" +
                "                                GROUP BY ProjectCode\r\n");

            //JSMsg("Past split");
            if (long != -1) {
                var Left = GetContracts.Row[count].TotalTonnage - long.Row[0].StockQuantity;
                sheet1.cells(count + lastline, 7).Value = long.Row[0].Positions;
            }
            else {
                var Left = GetContracts.Row[count].TotalTonnage
            }

            sheet1.cells(lastline, 3).Value = Left;
            var sum = 0;

            cntx = "Get all approved finance calcs"
            var Financed = sql.Fill("SELECT CT.ProjectCode, ISNULL(G.[Description],\'\') [Grade], ISNULL(tdf.StockQuantity,0)[Finance], SL.GradeNo, AV.Value from SLAccount SL\r\n" +
                "JOIN Contract CT on CT.ContractNo = SL.ContractNo\r\n" +
                "JOIN TransDet_FINANCIALS TDF on TDF.SLAccCode = SL.SLAccCode AND TDF.SourceProjectCode = CT.ProjectCode\r\n" +
                "LEFT JOIN AttribValue AV on TDF.TransNo = AV.Value AND AV.AttributeCode = \'BuyBack\'\r\n" +
                "LEFT JOIN Grade G on G.GradeNo = SL.GradeNo AND G.CommodityTypeNo = sl.CommodityTypeNo  \r\n" +
                "WHERE CT.ContractNo = " + GetContracts.Row[count].ContractNo + " AND TDF.Parent = 0 AND (TDF.StockQuantity > 0 AND TransTypeNo = 100 OR (TDF.TransNo = AV.Value AND AV.AttributeCode = \'BuyBack\'AND TransTypeNo = 102))\r\n");

            if (Financed != -1) {
                //Read each fincance against the contract
                for (index = 0; index < Financed.Row.length; index++) {
                    var financeTon = new Number(Financed.Row[index].Finance);
                    sheet1.cells(lastline, 2).Value = Financed.Row[index].Grade;
                    sheet1.cells(lastline, 4).Value = Financed.Row[index].Finance;
                    sheet1.cells(lastline, 3).Value = Left;
                    //Calculate tonnage left to finance against
                    Left -= financeTon;
                    sheet1.cells(lastline, 5).Value = Left;
                    lastline++;
                    //Sum the total finaced against the contract
                    sum += financeTon;
                }
            }
            //To create summery and extra space
            sheet1.cells(lastline, 3).Value = "Total per FSA contract"
            sheet1.cells(lastline, 4).Value = sum;
            sheet1.cells(lastline, 5).Value = Left;
            //Format summery
            sheet1.cells(lastline, 3).Font.Bold = "True";
            sheet1.cells(lastline, 4).Font.Bold = "True";
            sheet1.cells(lastline, 5).Font.Bold = "True";
            sheet1.cells(lastline, 3).Borders(8).LineStyle = 1
            sheet1.cells(lastline, 4).Borders(8).LineStyle = 1
            sheet1.cells(lastline, 5).Borders(8).LineStyle = 1
            sheet1.cells(lastline, 6).Borders(8).LineStyle = 1
            sheet1.cells(lastline, 7).Borders(8).LineStyle = 1

            sheet1.cells(lastline, 3).Borders(9).LineStyle = 1
            sheet1.cells(lastline, 4).Borders(9).LineStyle = 1
            sheet1.cells(lastline, 5).Borders(9).LineStyle = 1
            sheet1.cells(lastline, 6).Borders(9).LineStyle = 1
            sheet1.cells(lastline, 7).Borders(9).LineStyle = 1
            lastline += 2;
        }

        sheet1.Columns.Autofit;
        lastline += 2;
        offsetLine = lastline +5
    }
    else {
        JSMsg("No FSA found on this project");
    }
}
//#endregion