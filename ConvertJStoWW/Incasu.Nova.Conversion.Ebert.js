class SomeClass {
    constructor (pA, pB) {
        this.a = pA;
        this.b = pB;
    }
    toString () {
        return JSON.stringify ({"c": `${this.a}/${this.b}`})
    }
}
class AXEPage {
    constructor (pID, pTitle, pDescription) {
        this.id = pID;
        this.title = pTitle;
        this.description = pDescription;
    }
}
class ActiveXObject {
    constructor (pProgID) {
        this.progid = pProgID;
        this.obj = [];
        this.pages = [];
    }
    AddEventObject (pName, pObj) {
        this.obj.push ({'name': pName, 'obj': pObj});
    }
    AddPage (pID, pTitle, pDescription) {
        let p = new AXEPage(pID, pTitle, pDescription);
        this.pages.push (p);
        return p;
    }
}
var s = `
    var sc = new SomeClass ('AAA', 'BBB');
    var wiz = new ActiveXObject ('AXE.Wizard'); //TODO: Search string to replace name.
    var Glo = {"o": 123};
    wiz.AddEventObject ('Glo', Glo);
    function MyFunction () {return 'something'};
    function MyEnterFunction () {return 'something else'};
    var info = wiz.AddPage("info","Leave","Team Leader Leave Approval");
    info.enter = MyEnterFunction;
    info.exit = MyFunction;
`;
eval (s);

console.log(wiz);

//#region Eckard

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if(false){
    const Wizard = require('./web-wizard/classes/wizard.js');
    const app = new Wizard.App({ wwWizardData: wwWizardData, webServiceUrl: 'http://blnovadev.incasudynamics.co.za', primaryColor: 'rgb(223, 39, 39)' });
    const page1 = new Wizard.Page({ title: 'Page 1 Title', description: 'Page 1 Description' });
    app.addPage(page1);

    page1.addControl(new Wizard.Control({ id: 'scriptName', type: 'text', text: 'Enter name of script File to be created'}));
    page1.addControl(new Wizard.Control({ id: 'p1FilePicker', type: 'file', text: 'Pick the script to convert', onChange: 'p1FileChanged()' }));
    page1.addControl(new Wizard.Control({ id: 'p1Button', type: 'button', text: 'Convert Script  ', onClick: 'on1ButtonClick()' }));

    app.script = () => {
        //#region Global Objects
        var glo = new Object;
        glo.dataToWrite = '';
        glo.lineNo = '';
        glo.fn = '';
        //#endregion
        //#region Functions
        $scope.p1FileChanged = async function(e) {
            glo.files = e.target.files[0];
        }       
        $scope.on1ButtonClick = function() {
            if($scope.scriptName.value){
                glo.fileName = $scope.scriptName.value;
            }else{
                glo.fileName = 'newScript';
            }
            var fileReader = new FileReader();
            fileReader.readAsText(glo.files);
            fileReader.onload = () => {
                glo.text = fileReader.result;
                $scope.startReading();
            };
        }
        $scope.startReading = function(){
            var lines = glo.text.split(/[\r\n]+/g);
            for (var l = 0; l < lines.length; l++) {
                glo.lineNo = l;
                if(lines[l].indexOf("AddField") > 0){
                    var res = 'glo.' + lines[l];
                    eval(res);
                    continue;  
                }
                if(lines[l].indexOf("SetItems") > 0){
                    //do nothing  handeling it under addfield.
                    continue;
                }
                if(lines[l].indexOf("ExitScript") > 0){
                    //do nothing handeling it under addpage.
                    continue;
                }
                    eval(lines[l]);                  
            }      
            makeTextFile();
        }
        function ActiveXObject(n){
            this.Name = n;
            if(this.Name == "AXE.Wizard"){
                glo.dataToWrite = glo.dataToWrite.concat("const Wizard = require('./web-wizard/classes/wizard.js');\r\n");
                glo.dataToWrite = glo.dataToWrite.concat("const app = new Wizard.App({ wwWizardData: wwWizardData, webServiceUrl: 'http://blnovadev.incasudynamics.co.za' });\r\n\r\n");
            }
            this.AddPage = function(pId,pDescription,pTitle){
                        
            glo.dataToWrite  = glo.dataToWrite.concat("const " +  pId + "= new Wizard.Page({ title: '" + pTitle + "',description: '" + pDescription + "' });\r\n");
            glo.dataToWrite  = glo.dataToWrite.concat("app.addPage(" + pId  + ");\r\n");
            //add page as a glo var. 
            var newPage = 'glo.'+ pId + '= new ActiveXObject("'+pId+'");'
            eval(newPage);
            this.ExitScript(pId);
            }
            this.AddField = function(fName,fTitle,fType){
                fType = fType.toLowerCase();
                switch(fType){
                    case 'combo':
                        fType = 'dropdown';
                        glo.dataToWrite  = glo.dataToWrite.concat(this.Name + ".addControl(new Wizard.Control({ id: '" + fName + "', type: '" + fType + "', text: '" + fTitle + "'}));\r\n");
                        var lines = glo.text.split(/[\r\n]+/g);
                        for (var l = glo.lineNo; l < lines.length; l++) {
                            if(lines[l].indexOf("SetItems") > 0){
                                if(lines[l].indexOf(fName) > 0){
                                    var res = 'glo.' + lines[l];
                                    eval(res);
                                    break;
                                }
                            }           
                        }
                    break;
                    case 'text':
                        glo.dataToWrite  = glo.dataToWrite.concat(this.Name + ".addControl(new Wizard.Control({ id: '" + fName + "', type: '" + fType + "', text: '" + fTitle + "'}));\r\n");
                    break;
                    case 'check':
                        fType = 'checkbox';
                        glo.dataToWrite  = glo.dataToWrite.concat(this.Name + ".addControl(new Wizard.Control({ id: '" + fName + "', type: '" + fType + "', text: '" + fTitle + "'}));\r\n");
                    break;
                    case 'choice':
                    
                    break;
                    case 'date':
                        glo.dataToWrite  = glo.dataToWrite.concat(this.Name + ".addControl(new Wizard.Control({ id: '" + fName + "', type: '" + fType + "', text: '" + fTitle + "'}));\r\n");
                    break;
                    case 'file':
                        glo.dataToWrite  = glo.dataToWrite.concat(this.Name + ".addControl(new Wizard.Control({ id: '" + fName + "', type: '" + fType + "', text: '" + fTitle + "'}));\r\n");
                    break;
                    case 'folder':
                    break;
                    case 'spin':
                        fType = 'number';
                        glo.dataToWrite  = glo.dataToWrite.concat(this.Name + ".addControl(new Wizard.Control({ id: '" + fName + "', type: '" + fType + "', text: '" + fTitle + "'}));\r\n");
                    break;
                    case 'grid':
                        glo.dataToWrite = glo.dataToWrite.concat(this.Name + ".addControl(new Wizard.Table({ id: '" + fName + "', multiple: true, text:'" + fTitle + "',headers: [{ key: 'name', caption: 'The Name' }, { key: 'surname', caption: 'The Surname' }] }));\r\n");
                    
                    break;
                }//end of switch field type.            
            }
            this.SetItems = function(dName,dItems){
                var res = dItems.split("|");
                var options   = '';
                var buildFn =  '';
                for (var d = 0; d < res.length; d++) {
                    switch(d){
                        case 0:
                            options = options.concat("options: ['" + res[d] + "'");
                        break;
                        default:
                            options = options.concat(",'" + res[d] + "'");
                    }
                }
                options = options.concat(']');
                buildFn = buildFn.concat('$scope.' + dName + " = {\r\n");
                buildFn = buildFn.concat(options + '\r\n');
                buildFn = buildFn.concat('};\r\n\r\n');
                glo.fn = glo.fn.concat(buildFn);
            }//end of sentitems

            this.setTitle = function(){

            }//end of setTitles

            this.ExitScript = function(pId){
                var lines = glo.text.split(/[\r\n]+/g);
                var buildFn =  '';
                for (var l = glo.lineNo; l < lines.length; l++) {
                    if(lines[l].indexOf("ExitScript") > 0){
                            buildFn = buildFn.concat('$scope.' + pId + "Next" + " =  function(){\r\n");
                            buildFn = buildFn.concat('};\r\n\r\n');
                            glo.fn = glo.fn.concat(buildFn);
                            break;
                    }
                }
            }
            this.Start = function(){
                    glo.dataToWrite = glo.dataToWrite.concat("app.script = () => {\r\n");
                    glo.dataToWrite = glo.dataToWrite.concat(glo.fn);
                    glo.dataToWrite = glo.dataToWrite.concat("};\r\n");
                    glo.dataToWrite = glo.dataToWrite.concat("app.toHTMLFile();");
            }
        }
        function SQLHandler(c){
            this.Fill = function(sql){
                var txt = '';
                var variable = '';
                var lines = glo.text.split(/[\r\n]+/g);
                for (var c = 0; c < lines[glo.lineNo].length; c++) {
                    if (lines[glo.lineNo].charAt(c) == '='){
                        variable = txt.replace('var','');
                        break;
                    }
                    txt = txt.concat(lines[glo.lineNo].charAt(c));
                }
                glo.dataToWrite = glo.dataToWrite.concat("DataCall('" + variable + "','" + sql + "')\r\n");
            }
            this.GetScalar = function(sql){
                var txt = '';
                var variable = '';
                var lines = glo.text.split(/[\r\n]+/g);
                for (var c = 0; c < lines[glo.lineNo].length; c++) {
                    if (lines[glo.lineNo].charAt(c) == '='){
                        variable = txt.replace('var','');
                        break;
                    }
                    txt = txt.concat(lines[glo.lineNo].charAt(c));
                }
                glo.dataToWrite = glo.dataToWrite.concat("DataCall('" + variable + "','" + sql + "')\r\n");
            }
        }
        function Conn(c){
            this.Conn = c;
        }
        makeTextFile = function(){
            var element = document.createElement('a'); 
            element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(glo.dataToWrite));
            element.setAttribute('download', glo.fileName);
            element.style.display = 'none';
            document.body.appendChild(element);
            element.click();
            document.body.removeChild(element);
        }
    //#endregion
    };
    app.toHTMLFile();
}

//#endregion