class SomeClass {
    constructor (pA, pB) {
        this.a = pA;
        this.b = pB;
    }
    toString () {
        return JSON.stringify ({"c": `${this.a}/${this.b}`})
    }
}
class AXEFields{
    constructor(pID, pName, pType, pProperties){
        this.id = pID;
        this.name = pName;
        this.type= pType;
        this.properties = pProperties;
    }
}
class AXEPage {
    constructor (pID, pTitle, pDescription) {
        this.id = pID;
        this.title = pTitle;
        this.description = pDescription;
        this.fields = [];
    }
    AddField(pID, pName, pType, pProperties){
        this.fields.push(new AXEFields(pID, pName, pType, pProperties));
    }
}
class ActiveXObject {
    constructor (pProgID) {
        this.progid = pProgID;
        this.obj = [];
        this.pages = [];
    }
    AddEventObject (pName, pObj) {
        this.obj.push ({'name': pName, 'obj': pObj});
    }
    AddPage (pID, pTitle, pDescription) {
        let p = new AXEPage(pID, pTitle, pDescription);
        this.pages.push (p);
        return p;
    }
}
var s = `
    var sc = new SomeClass ('AAA', 'BBB');
    var wiz = new ActiveXObject ('AXE.Wizard');
    var Glo = {"o": 123};
    wiz.AddEventObject ('Glo', Glo);
    function MyFunction () {return 'something'};
    function MyEnterFunction () {return 'something else'};
    var info = wiz.AddPage("info","Leave","Team Leader Leave Approval");
    info.enter = MyEnterFunction;
    info.exit = MyFunction;
`;
eval (s);

console.log (wiz.pages[0].enter());