// Script to eval
var script = `
    var o = {};
    o.a = 123;
    o.b = something;
    o.c = somethingElse;
    console.log (JSON.stringify (o));
`;
// PreParse function
function preParse (pScript) {
    let ret = {script: null, added: [], errors: []};
    let cnt = 0;
    while (cnt++ < 100) {
        // Eval
        let msg = null;
        try {
            eval (pScript);
        }
        catch (err) {
            msg = err.message;
        }
        // Test if successful
        if (!msg) break;
        // Search for solution
        while (true) {
            // Scenario 1 - object is not defined
            let s = msg.split (' ');
            if (s.length == 4 && s[1] == 'is' && s[2] == 'not' && s[3] == 'defined') {
                // Add object to script to eval again
                pScript = `let ${s[0]} = {};\r\n` + pScript;
                ret.added.push (s[0]);
                break;
            }
            // Scenario 2 - next error type
            // test and do something
            break;
            // Scenario 3 - next error type
            // test and do something
            break;
            // Default - unhandled error
            res.errors.push (msg);
            break;
        }
    }
    if (cnt >= 100) res.errors.push ('More than 100 errors encountered');
    // Return
    pScript = `//\n//PREPARSED ${ret.added.length != 0 || ret.errors.length != 0 ? '(some changes made or errors encountered, attention needed)' : ''}\n//\n\n${pScript}`;
    ret.script = pScript;
    return ret;
}
// Report
console.log ('parsed:\n\n', preParse (script));