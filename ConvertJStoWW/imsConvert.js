const Wizard = require('./web-wizard/classes/wizard.js');

// wwWizardData will be filled in automatically
const app = new Wizard.App({ wwWizardData: wwWizardData, webServiceUrl: 'http://blnovadev.incasudynamics.co.za', primaryColor: 'rgb(223, 39, 39)' });

// Data calls
    // app.addDataCall({type: 'microsoftsql', data: {
    //     id: 'getVebiClient',
    //     user: 'imsWebWizard',
    //     password: '@imsWebWizard123',
    //     server: 'localhost', 
    //     port: '49722',
    //     database: 'Vebi',
    //     query: `SELECT * FROM "Client" WHERE "GUID" = '$GUID$'`
    // }});        

    app.addDataCall({type: 'web', data: {
        id: 'getRuns',
        method: 'get',
        url: 'http://160.119.100.45:3700/dbrun'
    }});

    app.addDataCall({
        type: 'web', data: {
            id: 'getAllRuns',
            method: 'post',
            url: `${environmentContext.Url.BusinessLayer}/utConvertAllRuns`
        }
    });

    //utConvertAllRuns
    //utConvertStartRun

    // app.addDataCall({type: 'web', data: {    
    //     id: 'postNewFeedback',    
    //     method: 'post',    
    //     // headers: {'content-type': 'application/json'}, // optional    
    //     url: 'http://160.119.101.133:2105/utCreateFeedback',    
    //     body: `{        
    //         "ProductID": "$ProductID$",        
    //         "Username": "$Username$",        
    //         "Email": "$Email$",        
    //         "Type": "$Type$",        
    //         "Body": "$Body$"    
    //     }` 
    //     // only for method:'post' or method:'put'
    //     }
    // });
    // app.addDataCall({type: 'web', data: {
    //     id: 'postNewFeedback',
    //     method: 'post',
    //     // headers: {'content-type': 'application/json'}, // optional
    //     url: 'http://160.119.101.133:2105/utCreateFeedback',
    //     body: `{
    //         "ProductID": "$ProductID$",
    //         "Username": "$Username$",
    //         "Email": "$Email$",
    //         "Type": "$Type$",
    //         "Body": "$Body$"
    //     }` // only for method:'post' or method:'put'
    // }});

const page1 = new Wizard.Page({ title: 'Page 1 Title', description: 'Page 1 Description' });
// const page2 = new Wizard.Page({ title: 'Page 2 Title', description: 'Page 2 Description' });
// const page3 = new Wizard.Page({ title: 'Page 3 Title', description: 'Page 3 Description' });

app.addPage(page1);
// app.addPage(page2);
// app.addPage(page3);

page1.addControl(new Wizard.Control({ id: 'btnGetResult', type: 'button', widthPercentage: '30', text: 'GetData', onClick: 'p1GetData()' }));

app.script = () => {

    $scope.p1GetData = async function(){
        const response = await wwDoDataCall({
            id: 'getAllRuns'
        });
        console.log(response);
    }

    // const asyncPutAndPostHTTPCall = async function() {    
    //     const response = await wwDoDataCall({ id: 'getRuns', parameters: {        
    //         ProductID: 'f4ea56b8-68f8-4b48-a87b-2cf0af8f30a2',
    //         Username: 'THE MAN',        
    //         Email: 'francoisk@incasu.co.za',
    //         Type: 'Bug',        
    //         Body: 'THE MAN, found a bug!',    
    //         } 
    //     });    
    //     console.log(JSON.parse(response)); // For some reason I need to parse this twice?
    // };
        


};

app.toHTMLFile();